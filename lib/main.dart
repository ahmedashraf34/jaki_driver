import 'package:bot_toast/bot_toast.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:jaki_kapten/ToolsApp/app_provider.dart';
import 'package:jaki_kapten/services/Locator.dart';
import 'package:jaki_kapten/services/exports.dart';
import 'package:provider/provider.dart';

import 'notifications/firebase_notification.dart';
import 'notifications/local_notifications.dart';
import 'services/static_functions.dart';

void main()async {
  WidgetsFlutterBinding.ensureInitialized();
  await setupLocator();

  StaticFunctions.getLocation();
  await Firebase.initializeApp();
  await PrefManager.initializePrefData();
  await LocalNotifications.initialize();
  // await FirebaseNotifications.init();

  runApp(ChangeNotifierProvider<AppProvider>(
      create: (_) => AppProvider(), child: MyApp()));
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  int data;
  @override
  Widget build(BuildContext context) {
    return BotToastInit(
      child: MaterialApp(
        navigatorObservers: [BotToastNavigatorObserver()],

        debugShowCheckedModeBanner: false,
        home: Splash(havedata: PrefManager.currentUser.device_token.isNotEmpty?1:null),
        locale: const Locale('ar'),
        theme: ThemeData(
          fontFamily: 'cairo',
          unselectedWidgetColor: Colors.white,
        ),
      ),
    );
  }
}
