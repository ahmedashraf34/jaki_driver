import 'package:flutter/services.dart';
import 'package:jaki_kapten/services/exports.dart';
import 'package:jaki_kapten/services/static_functions.dart';
import 'package:location/location.dart';

class HomeOnePage extends StatefulWidget {
  double recLatPick, recLngPick, recLatdrop, recLngdrop;
  String tripId;

  HomeOnePage(
      {Key key,
      this.recLatPick,
      this.recLngPick,
      this.recLatdrop,
      this.recLngdrop,
      this.tripId})
      : super(key: key);

  HomeOnePageState createState() => new HomeOnePageState();
}

class HomeOnePageState extends State<HomeOnePage> {
  Timer timer;
  String title = 'تنبيه هام ';
  String subtitle = 'من فضلك قم بالضغط علي هذا التنبة لاستكمال الملف الشخصي وبيانات المركبه لتتمكن من استخدام التطبيق  ';
  num Rate, TripsCount, TripsCountOfToday, TotalEarning, TotalEarningOfToday;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  //double lat, lng;
  bool getStat = false;
  StreamSubscription _locationSubscription;
  Location _locationTracker = Location();
  List<Marker> allMarkers = [];
  Circle circle;
  BitmapDescriptor imagepick, imagedrop, drivimage;

  void _listenLocation() async {
    try {
      if (_locationSubscription != null) {
        _locationSubscription.cancel();
      }

      _locationSubscription =
          _locationTracker.onLocationChanged().listen((newLocalData) {
        if (_controller != null) {
          _controller.animateCamera(CameraUpdate.newCameraPosition(
              new CameraPosition(
                  bearing: 192.8334901395799,
                  target: LatLng(newLocalData.latitude, newLocalData.longitude),
                  tilt: 0,
                  zoom: 18.00)));
        }
      });
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        debugPrint("Permission Denied");
      }
    }
  }

  Future<void> _stopListen() async {
    _locationSubscription.cancel();
  }

  @override
  void initState() {
    loginAPI(
        userName: PrefManager.currentUser.user_name,
        passWord: PrefManager.currentUser.password,
        lang: null,
        devId: PrefManager.currentUser.device_token,
        devType: null,
        driType: PrefManager.currentUser.type
    );
    _listenLocation();
      allMarkers.add(Marker(
          markerId: MarkerId('pickMarker'),
          draggable: false,
          onTap: () {
            tostDialog("نقطة تواجد المستخدم");
          },
          position: LatLng(widget.recLatPick??StaticFunctions.lat, widget.recLngPick??StaticFunctions.lng)));
      allMarkers.add(Marker(
          markerId: MarkerId('dropMarker'),
          draggable: false,
          onTap: () {
            print('النقطة النهائية');
          },
          position: LatLng(widget.recLatdrop??StaticFunctions.lat, widget.recLngdrop??StaticFunctions.lng)));
    timer = Timer.periodic(Duration(seconds: 60), (timer) {
          pushDriverLocation(StaticFunctions.lat ?? '', StaticFunctions.lng ?? '');
          print("hiiiiiiiii");
        });
      driverStatistics().then((userStati) {
        if(userStati != null) {
          setState(() {
            Rate = userStati[0];
            TripsCount = userStati[1];
            TripsCountOfToday = userStati[2];
            TotalEarning = userStati[3];
            TotalEarningOfToday = userStati[4];
          });
        }
      });

    super.initState();
  }

  GoogleMapController _controller;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      endDrawer: MyDrawer(),
      appBar: AppBar(
          elevation: 0,
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.menu,
                color: Colors.black54,
              ),
              onPressed: () {
                _scaffoldKey.currentState.openEndDrawer();
              },
            ),
          ],
          backgroundColor: Colors.white,
          title: Row(children: <Widget>[
            SizedBox(
              width: 25,
            ),
            Switch(
              onChanged: (value) {
                setState(() {
                  getStat = !getStat;
                });
              },
              value: getStat,
            ),
            SizedBox(
              width: 15,
            ),
            Center(
                child: Image.asset(
              "assets/images/logo2.png",
              width: 80,
            )),
            SizedBox(
              width: 15,
            ),
            InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => MessagesPage()));
                },
                child: Icon(
                  Icons.mail,
                  color: Colors.black54,
                  size: 24,
                ))
          ]),
          leading: new IconButton(
            icon: Icon(
              Icons.notifications,
              color: Colors.black54,
            ),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => NotificationsPAge()));
            },
            color: Colors.black54,
            iconSize: 30,
          )),
      body: StaticFunctions.lat != 0.0 || StaticFunctions.lng != 0.0
          ? Stack(
            overflow: Overflow.visible,
            alignment: Alignment.center,
            children: <Widget>[
              //============================== الجزء الخاص بالخريطة ===
              Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  color: Colors.white,
                  // image: DecorationImage(fit: BoxFit.cover,colorFilter: ColorFilter.linearToSrgbGamma(),image: AssetImage("assets/images/map.png"))
                ),
                child: GoogleMap(
                  myLocationEnabled: true,
                  mapType: MapType.normal,
                  zoomControlsEnabled: false,
                  //zoomGesturesEnabled: false,
                  initialCameraPosition: CameraPosition(
                    target: LatLng(StaticFunctions.lat, StaticFunctions.lng),
                    zoom: widget.recLatdrop == null ? 15 : 10,
                  ),
                  markers: Set.from(allMarkers != null ? allMarkers : []),
                  circles: Set.of((circle != null) ? [circle] : []),
                  onMapCreated: (GoogleMapController controller) {
                    _controller = controller;
                  },
                ),
              ),

              getStat == true
                  ? FittedBox(
                    child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white.withOpacity(0.8),
                          borderRadius: BorderRadius.circular(12.0)
                        ),
                        padding: const EdgeInsets.all(10.0),
                        //height: MediaQuery.of(context).size.height * 0.55,
                        child: Column(
                          children: <Widget>[
                            AvatarGlow(
                              glowColor: Colors.blue,
                              endRadius: 90.0,
                              duration: Duration(milliseconds: 2000),
                              repeatPauseDuration:
                                  Duration(milliseconds: 100),
                              child: Material(
                                elevation: 2,
                                shape: CircleBorder(),
                                child: CircleAvatar(
                                  backgroundColor: Colors.grey[100],
                                  radius: 55.0,
                                  child: GestureDetector(
                                    child: Container(
                                      decoration: BoxDecoration(
                                          color: Colors.grey,
                                          borderRadius:
                                              BorderRadius.circular(55),
                                          image: DecorationImage(
                                              fit: BoxFit.cover,
                                              image: AssetImage(
                                                  'assets/images/user.png'))),
                                    ),
                                    onTap: () {
                                      TripsCount != null
                                          ? tostDialog(
                                              "$TripsCountعدد رحلاتك ")
                                          : tostDialog(
                                              "لا توجد رحلات لك");
                                    },
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 70,
                            ),
                            SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Column(
                                    children: <Widget>[
                                      CircleAvatar(
                                          backgroundColor: anGrey,
                                          maxRadius: 40,
                                          child: Image.asset(
                                            "assets/images/rate.png",
                                            scale: 3,
                                          )),
                                      Rate != null
                                          ? Text(" ر.ص$Rate")
                                          : Text("لايوجد تقييم"),
                                      Rate != null
                                          ? Text("تقييم")
                                          : Container()
                                    ],
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Column(
                                    children: <Widget>[
                                      CircleAvatar(
                                          backgroundColor: anGrey,
                                          maxRadius: 40,
                                          child: Image.asset(
                                            "assets/images/cost.png",
                                            scale: 12,
                                          )),
                                      TotalEarning != null
                                          ? Text(" ر.ص$TotalEarning")
                                          : Text("لاتوجد ارباح"),
                                      TotalEarning != null
                                          ? Text("اجمالي الارباح")
                                          : Container()
                                    ],
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Column(
                                    children: <Widget>[
                                      CircleAvatar(
                                          backgroundColor: anGrey,
                                          maxRadius: 40,
                                          child: Image.asset(
                                            "assets/images/money.png",
                                            scale: 10,
                                          )),
                                      TotalEarningOfToday != null
                                          ? Text(" ر.ص$TotalEarningOfToday")
                                          : Text("لاتوجد ارباح اليوم"),
                                      TotalEarningOfToday != null
                                          ? Text("ارباح اليوم")
                                          : Container(),
                                    ],
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Column(
                                    children: <Widget>[
                                      CircleAvatar(
                                          backgroundColor: anGrey,
                                          maxRadius: 40,
                                          child: Image.asset(
                                            "assets/images/trip.png",
                                            scale: 12,
                                          ),),
                                      TripsCountOfToday != null
                                          ? Text("$TripsCountOfToday")
                                          : Text("لا توجد رحلات اليوم"),
                                      TripsCountOfToday != null
                                          ? Text("رحلات اليوم")
                                          : Container()
                                    ],
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                  )
                  : Container(),
            ],
          )
          : Container(
              child: Center(child: Text('loading...')),
            ),
    );
  }

  @override
  void dispose() {
    timer.cancel();
    _stopListen();
    super.dispose();
  }
}
