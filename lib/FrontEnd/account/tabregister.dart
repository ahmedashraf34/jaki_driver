
import 'package:jaki_kapten/services/exports.dart';

class AccountTabsPage extends StatefulWidget {
  final int index;
  AccountTabsPage({this.index = 1});
  @override
  _AccountTabsPageState createState() => _AccountTabsPageState();
}

class _AccountTabsPageState extends State<AccountTabsPage> {
  FirebaseMessaging notificationToken = FirebaseMessaging();
  String f_c_m_Token;

  @override
  void initState() {
    notificationToken.getToken().then((token) {
      f_c_m_Token = token.toString();
      print(f_c_m_Token);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: widget.index,
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: anWhite,
          bottom: TabBar(
            labelColor: anRed,
            unselectedLabelColor: Colors.black,
            tabs: [
              Tab(
                text: 'انشاء حساب جديد',
              ),
              Tab(
                text: 'تسجيل الدخول',
              ),
            ],
          ),
          title: Container(
              margin: EdgeInsets.only(top: 15),
              child: Center(
                  child: Image.asset('assets/images/logo2.png', height: 50))),
        ),
        body: TabBarView(
          children: [
            RegisterPage(),
            LoginPage(),
          ],
        ),
      ),
    );
  }
}
