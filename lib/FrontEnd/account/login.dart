import 'package:dio/dio.dart';
import 'package:jaki_kapten/services/Models/UserData.dart';
import 'package:jaki_kapten/services/exports.dart';

class LoginPage extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<LoginPage> {
  TextEditingController userController = new TextEditingController();
  TextEditingController passController = new TextEditingController();
  TextEditingController mailController = new TextEditingController();
  FirebaseMessaging notificationToken = FirebaseMessaging();
  String f_c_m_Token;
  String radioItem = '';
  bool isLoading = false;

  @override
  void initState() {
    notificationToken.getToken().then((token) {
      f_c_m_Token = token.toString();
      print(f_c_m_Token);
    });
    setState(() {
      radioItem = 'كابتن';
    });
    super.initState();
  }

  void loading() {
    setState(() => isLoading = true);
  }

  void notLoading() {
    setState(() => isLoading = false);
  }




  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: anGrey,
      bottomNavigationBar: Container(
        padding: EdgeInsets.symmetric(vertical: 10),
        color: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            FlatButton(
                onPressed: () async {
                  await showDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                            title: Text('أدخل البريدالإلكتروني الخاص بك'),
                            content: TextField(
                              controller: mailController,
                              onSubmitted: (value) async {
                                if (value.trim() != '' && value.contains('@')) {
                                  loading();
                                  await forgetPassAPI(value, radioItem)
                                      .then((res) {
                                    setState(() {
                                      if (res != null)
                                        mailController.value =
                                            TextEditingValue(text: res);
                                      isLoading = false;
                                    });
                                  });
                                } else {
                                  tostDialog("أدخل بيانات صحيحة");
                                }
                                setState(() => mailController.clear());
                              },
                              keyboardType: TextInputType.emailAddress,
                              textAlign: TextAlign.start,
                              decoration: InputDecoration(
                                prefixIcon: Icon(Icons.mail),
                                hintText: 'mo98@ex.com',
                              ),
                            ),
                          ));
                },
                child: new Text("لااستطيع الدخول للحساب",
                    style: TextStyle(fontSize: 16))),
            new Image.asset(
              "assets/images/icone.png",
              width: 40,
            )
          ],
        ),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height - 250,
        padding: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: anGrey,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Card(
            child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              //------- صورة المستخدم
              new Container(
                height: 180,
                width: 180,
                decoration: BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.cover,
                      image: AssetImage("assets/images/profile2.png")),
                  borderRadius: BorderRadius.circular(100),
                ),
              ),

              SizedBox(height: 5),
              new Text(
                "سجل دخول لاستخدام التطبيق",
                style: TextStyle(fontSize: 16),
              ),

              SizedBox(height: 10),
              new Container(
                height: 50,
//                      color: Colors.grey,
                child: new TextField(
                  controller: userController,
                  keyboardType: TextInputType.text,
                  textAlign: TextAlign.end,
                  decoration: InputDecoration(
                    suffixIcon: Icon(Icons.email),
                    hintText: 'أسم المستخدم',
                    hintMaxLines: 1,
                  ),
                ),
              ),
              SizedBox(
                height: 5,
              ),
              new Container(
                height: 50,
//                      color: Colors.grey,
                child: new TextField(
                  controller: passController,
                  keyboardType: TextInputType.text,
                  textAlign: TextAlign.end,
                  decoration: InputDecoration(
                    suffixIcon: Icon(Icons.lock_outline),
                    hintText: 'كلمة المرور',
                    hintMaxLines: 1,
                  ),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text('سائق نقل'),
                      Radio(
                        groupValue: radioItem,
                        value: 'سائق نقل',
                        activeColor: Colors.red,
                        onChanged: (val) {
                          setState(() {
                            radioItem = val;
                            print('tttttttttt${radioItem}');
                          });
                        },
                      )
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Text('وايتات مياة'),
                      Radio(
                        groupValue: radioItem,
                        value: 'وايتات مياة',
                        activeColor: Colors.red,
                        onChanged: (val) {
                          setState(() {
                            radioItem = val;
                            print('tttttttttt${radioItem}');

                          });
                        },
                      )
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Text('كابتن'),
                      Radio(
                        groupValue: radioItem,
                        value: 'كابتن',
                        activeColor: Colors.red,
                        onChanged: (val) {
                          setState(() {
                            radioItem = val;
                            print('tttttttttt${radioItem}');

                          });
                        },
                      )
                    ],
                  ),
                ],
              ),
//              SizedBox(
//                height: 5,
//              ),
//              Text(
//                "هل نسيت كلمة المرور ؟",
//                textAlign: TextAlign.left,
//              ),
              SizedBox(height: 15),
              my_Button(
                  horizontal: 10,
                  textButton: "دخول",
                  onBtnclicked: () async {
                    var token = await PrefManager.getFireBaseToken();
                    print('teeeeeeeeeeeeeeest${token}');
                    if (passController.text.trim() != "" &&
                        userController.text.trim() != "") {
                      loading();
                      await loginAPI(
                              userName: userController.text,
                              passWord: passController.text,
                              lang: null,
                              devId: token,
                              devType: null,
                              driType: radioItem == 'سائق نقل'
                                  ? 'Trans'
                                  : radioItem == 'وايتات مياة'
                                      ? 'Waitat'
                                      : 'Driver')
                          .then((v) async {

                        notLoading();

                        if (v != null) {

                          // سائق نقل - وايتات مياة - كابتن
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => HomeOnePage()));
                        }
                      });
                    }
                  },
                  colorButton: anRed,
                  fontSize: 16,
                  heightButton: 60,
                  radiusButton: 10),
              SizedBox(height: 30.0),
              isLoading
                  ? Center(child: CircularProgressIndicator())
                  : Container()
            ],
          ),
        )),
      ),
    );
  }
}
