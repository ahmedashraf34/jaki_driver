import 'package:jaki_kapten/services/exports.dart';

class RegisterPage extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<RegisterPage> {
  TextEditingController phone = new TextEditingController();
  TextEditingController firstName = new TextEditingController();
  TextEditingController lastName = new TextEditingController();
  TextEditingController email = new TextEditingController();
  TextEditingController pass = new TextEditingController();
  TextEditingController rePass = new TextEditingController();
  String radioItem = '', UserName = '';
  bool isLoading = false;

  void loading(){
    setState(() => isLoading = true);
  }
  void notLoading(){
    setState(() => isLoading = false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: anGrey,
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height - 170,
          // margin: EdgeInsets.symmetric(horizontal: 10, vertical: 70),
          padding: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: anGrey,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Card(
              child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                SizedBox(height: 40),
                Padding(
                  padding: const EdgeInsets.only(left: 12),
                  child: new Row(
                    children: <Widget>[
                      //===========================الاسم الاخير =====
                      Expanded(
                          flex: 1,
                          child: new Container(
                            height: 50,
                            child: new TextField(
                              controller: lastName,
                              keyboardType: TextInputType.text,
                              textAlign: TextAlign.end,
                              onChanged: (value) {
                                setState(() {
                                  UserName =
                                      (firstName.text + lastName.text) ??
                                          '';
                                });
                              },
                              decoration: InputDecoration(
                                suffixIcon: Icon(Icons.supervisor_account),
                                hintText: 'الاسم الاخير',
                                hintMaxLines: 1,
                              ),
                            ),
                          )),

                      SizedBox(width: 15),
                      //===========================الاسم الاول =====
                      Expanded(
                          flex: 1,
                          child: new Container(
                            height: 50,
                            child: new TextField(
                              controller: firstName,
                              keyboardType: TextInputType.text,
                              textAlign: TextAlign.end,
                              onChanged: (value) {
                                setState(() {
                                  UserName =
                                      (firstName.text + lastName.text) ??
                                          '';
                                });
                              },
                              decoration: InputDecoration(
                                suffixIcon: Icon(Icons.supervisor_account),
                                hintText: 'الاسم الاول',
                                hintMaxLines: 1,
                              ),
                            ),
                          )),
                    ],
                  ),
                ),
                SizedBox(height: 10),
                Text('أسم المستخدم : $UserName'),
                SizedBox(height: 40),
                Padding(
                  padding: const EdgeInsets.only(left: 12),
                  child: new Row(
                    children: <Widget>[
                      //=مفتاح الدولة===============================
                      Expanded(
                          flex: 2,
                          child: new Container(
                            height: 40,
//                      color: Colors.grey,
                            child: new Text(
                              "+966",
                              style: TextStyle(fontSize: 20),
                            ),
                          )),

                      //=رقم الهاتف===============================
                      Expanded(
                          flex: 4,
                          child: new Container(
                            height: 40,
//                      color: Colors.grey,
                            child: new TextField(
                              controller: phone,
                              keyboardType: TextInputType.phone,
                              textAlign: TextAlign.end,
                              decoration: InputDecoration(
                                suffixIcon: Icon(Icons.phone_android),
                                hintText: 'الجوال',
                                hintMaxLines: 1,
                              ),
                            ),
                          )),
                    ],
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                //===========================password  =====
                new Container(
                  height: 50,
//                      color: Colors.grey,
                  child: new TextField(
                    controller: email,
                    keyboardType: TextInputType.emailAddress,
                    textAlign: TextAlign.end,
                    decoration: InputDecoration(
                      suffixIcon: Icon(Icons.email),
                      hintText: '  البريد الالكتروني',
                      hintMaxLines: 1,
                    ),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                //===========================password  =====
                new Container(
                  height: 50,
//                      color: Colors.grey,
                  child: new TextField(
                    controller: pass,
                    keyboardType: TextInputType.text,
                    textAlign: TextAlign.end,
                    decoration: InputDecoration(
                      suffixIcon: Icon(Icons.lock_outline),
                      hintText: ' كلمة المرور',
                      hintMaxLines: 1,
                    ),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                //===========================password=====
                new Container(
                  height: 50,
//                      color: Colors.grey,
                  child: new TextField(
                    controller: rePass,
                    keyboardType: TextInputType.text,
                    textAlign: TextAlign.end,
                    decoration: InputDecoration(
                      suffixIcon: Icon(Icons.lock_outline),
                      hintText: 'تاكيد كلمة المرور',
                      hintMaxLines: 1,
                    ),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text('سائق نقل'),
                        Radio(
                          groupValue: radioItem,
                          value: 'سائق نقل',
                          activeColor: Colors.red,
                          onChanged: (val) {
                            setState(() {
                              radioItem = val;
                            });
                          },
                        )
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Text('وايتات مياة'),
                        Radio(
                          groupValue: radioItem,
                          value: 'وايتات مياة',
                          activeColor: Colors.red,
                          onChanged: (val) {
                            setState(() {
                              radioItem = val;
                            });
                          },
                        )
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Text('كابتن'),
                        Radio(
                          groupValue: radioItem,
                          value: 'كابتن',
                          activeColor: Colors.red,
                          onChanged: (val) {
                            setState(() {
                              radioItem = val;
                            });
                          },
                        )
                      ],
                    ),
                  ],
                ),
                //                ==============================زر امتلك حساب==
                SizedBox(height: 40),
                my_Button(
                    horizontal: 10,
                    textButton: "امتلك حساب",
                    onBtnclicked: () async{
                      if (firstName.text.trim() != "" &&
                          lastName.text.trim() != "" &&
                          email.text.trim() != "" &&
                          phone.text.trim() != "" &&
                          pass.text == rePass.text &&
                          pass.text.trim() != "") {
                        loading();
                        await registerDriverAPI(
                            eMail: email.text,
                            firstName: firstName.text,
                            passWord: pass.text,
                            lastName: lastName.text,
                            phoneNumber: phone.text,
                            drivType: radioItem == 'سائق نقل'?'Trans':radioItem == 'وايتات مياة'?'Waitat':'Driver'
                        ).then((v) {
                          notLoading();
                          if (v != null) {
                            tostDialog('تم التسجيل بنجاح من فضلك سجل دخولك');
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => AccountTabsPage(index: 1,)));
                          }else{
                            tostDialog('رقم الهاتف مسجل بالفعل!');
                          }
                        });
                      }
                      else{
                        tostDialog("الرجاء إكمال البيانات حتى تتمكن من التسجيل");
                      }
                    },
                    colorButton: anRed,
                    fontSize: 18,
                    heightButton: 60,
                    radiusButton: 10),
                SizedBox(height: 40),
                isLoading?Center(child: CircularProgressIndicator()):Container()
              ],
            ),
          )),
        ),
      ),
    );
  }

  @override
  void initState() {
    setState(() {
      radioItem = 'كابتن';
    });
    super.initState();
  }
}
