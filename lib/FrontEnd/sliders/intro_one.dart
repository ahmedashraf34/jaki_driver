import 'package:jaki_kapten/services/exports.dart';

class FirstPage extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<FirstPage> {
  String image = 'assets/images/intro4.png';
  String title = "مرحباً بك في جاكي";
  String subtitle =
      "يوفر التطبيق طريقة سهلة وسريعة لتوصيل الراكب وجني المال بكل سهولة";

  void skip() async{
    PrefManager.setNotFirstTime(true);
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => GPSPage())
    );
  }

  void next() {
    pageController.jumpToPage(1);
  }

  @override
  Widget build(BuildContext context) {
    final double MQH = MediaQuery.of(context).size.height;
    return new Scaffold(
      body: Column(
        children: <Widget>[
          Stack(
            overflow: Overflow.visible,
            children: <Widget>[
              Container(
                  height: MQH * 0.445,
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.only(top: 60, left: 30),
                  color: anGrey,
                  child: InkWell(
                      onTap: skip,
                      child: Align(
                          alignment: Alignment.topLeft,
                          child: new Text(
                            "تخطي",
                            style: TextStyle(
                                fontSize: 16, color: Colors.black45),
                          )))
              ),
              Positioned(
                bottom: -60,
                right: 40,
                left: 40,
                child: new Container(
                  height: MQH * 0.41,
                  decoration: BoxDecoration(
                      image: DecorationImage(image: AssetImage("$image"))),
                ),
              ),
            ],
          ),
          SizedBox(
            height: MQH * 0.06,
          ),
          Text(
            title,
            style: TextStyle(fontSize: 18),
          ),
          SizedBox(
            height: MQH * 0.02,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
            child: new Text(subtitle,
                style: TextStyle(fontSize: 16, color: Colors.grey),
                textAlign: TextAlign.center),
          ),

          Expanded(
            child: Center(
              child: my_Button(
                  textButton: "التالي",
                  horizontal: 95,
                  radiusButton: 30,
                  heightButton: 60,
                  fontSize: 18,
                  onBtnclicked: () {
                    next();
                  },
                  colorButton: Colors.black),
            ),
          )
        ],
      ),
    );
  }
}
