import 'package:jaki_kapten/services/exports.dart';
class ThirdPage extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<ThirdPage> {
  String image = 'assets/images/intro5.png';
  String title = "حقق دخل اضافي  ";
  String subtitle =
      "بامكانك الاستفادة من وقت فراغك بتحقيق دخل اضافي من تطبيق جاكي كابتن";

  void skip() {
    print('Skip');
  }

  @override
  Widget build(BuildContext context) {
    final double MQH = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Column(
        children: <Widget>[
          Stack(
            overflow: Overflow.visible,
            children: <Widget>[
              Container(
                  height: MQH * 0.445,
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.only(top: 60, left: 30),
                  color: anGrey,
              ),
              Positioned(
                bottom: -60,
                right: 40,
                left: 40,
                child: Container(
                  height: MQH * 0.41,
                  decoration: BoxDecoration(
                      image: DecorationImage(image: AssetImage(image))),
                ),
              ),
            ],
          ),
          SizedBox(
            height: MQH * 0.06,
          ),
          Text(
            title,
            style: TextStyle(fontSize: 18),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
            child: new Text(subtitle,
                style: TextStyle(fontSize: 18, color: Colors.grey),
                textAlign: TextAlign.center),
          ),
          Expanded(
            child: Center(
              child: my_Button(
                  textButton: "إنهاء",
                  horizontal: 95,
                  radiusButton: 30,
                  heightButton: 60,
                  fontSize: 18,
                  onBtnclicked: () {
                    Navigator.pushReplacement(
                        context, MaterialPageRoute(builder: (context) => GPSPage()));
                  },
                  colorButton: Colors.black),
            ),
          )
        ],
      ),
    );
  }
}
