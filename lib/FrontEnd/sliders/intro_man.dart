
import 'package:jaki_kapten/services/exports.dart';

int page = 0;
PageController pageController =
    new PageController(initialPage: page, keepPage: true);

class MainPage extends StatefulWidget {
  @override
  _SliderState createState() => _SliderState();
}

class _SliderState extends State<MainPage> {
  static List<Widget> pages = [FirstPage(), SecondPage(), ThirdPage()];

  PageView pageView = new PageView(
    onPageChanged: (pageNumber) {
      page = pageNumber;
    },
    scrollDirection: Axis.horizontal,
    controller: pageController,
    children: pages,
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            height: MediaQuery.of(context).size.height,
            color: Colors.grey[200],
            child: Stack(
              children: <Widget>[
                Align(
                    child: PageIndicatorContainer(
                  pageView: pageView,
                  align: IndicatorAlign.bottom,
                  length: 3,
                  indicatorSpace: 10.0,
                  // padding: EdgeInsets.fromLTRB(10, 0, 10, 100),
                  indicatorColor: Colors.grey[200],
                  indicatorSelectorColor: Colors.black,
                  shape: IndicatorShape.circle(size: 12),
                )),
              ],
            )));
  }
}
