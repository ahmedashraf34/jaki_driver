import 'package:jaki_kapten/services/exports.dart';

class GPSPage extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<GPSPage> {
  String image = 'assets/images/intro1.png';
  String title = "تحديد الموقع  ";
  String subtitle = "من فضلك قم بتفعيل ال GPS من اجل الحصول علي اقصي استفادة من التطبيق واستقبال طلبات الركاب ";
  bool isLoading = false;


  @override
  void initState() {
    super.initState();
  }

  void loading(){
    setState(() => isLoading = true);
  }
  void notLoading(){
    setState(() => isLoading = false);
  }

  @override
  Widget build(BuildContext context) {
    final double MQH = MediaQuery.of(context).size.height;
    return Scaffold(
      bottomNavigationBar: BottomAppBar(
          child: my_Button(
              textButton: "GPS تفعيل ال",
              horizontal: 0,
              vertical: 0,
              radiusButton: 2,
              heightButton: 60,
              fontSize: 18,
              onBtnclicked: () async{
                loading();
                await getLocation().then((v) {
                  PrefManager.setNotFirstTime(true);
                  notLoading();
                  print("get the : $v");
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => AccountTabsPage())
                  );
                });
              },
              colorButton: anRed)),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Stack(
              overflow: Overflow.visible,
              children: <Widget>[
                Container(
                    height: MQH * 0.445,
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.only(top: 60, left: 30),
                    color: anGrey,
                ),
                Positioned(
                  bottom: -60,
                  right: 40,
                  left: 40,
                  child: new Container(
                    height: MQH * 0.41,
                    decoration: BoxDecoration(
                        image: DecorationImage(image: AssetImage("$image"))),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: MQH * 0.06,
            ),
            Text(
              "$title",
              style: TextStyle(fontSize: 18),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 20, right: 20, bottom: 20.0),
              child: new Text("$subtitle",
                  style: TextStyle(fontSize: 18, color: Colors.grey),
                  textAlign: TextAlign.center),
            ),

            isLoading?Center(child: CircularProgressIndicator()):Container(),
          ],
        ),
      ),
    );
  }
}
