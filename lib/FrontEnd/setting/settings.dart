import 'package:flutter/material.dart';
import 'package:jaki_kapten/ToolsApp/app_Colors.dart';
import 'package:jaki_kapten/services/exports.dart';

class SettingsPage extends StatefulWidget {

  @override
  _PaymentPageState createState() => _PaymentPageState();
}

class _PaymentPageState extends State<SettingsPage> {
  TextEditingController nameCON = new TextEditingController();
  TextEditingController phoneCON = new TextEditingController();
  TextEditingController emailCON = new TextEditingController();
  TextEditingController passwordCON = new TextEditingController();
  TextEditingController genderCON = new TextEditingController();
  var currentGValue;
  String token, id,type;
  final gTypes = ['Male', 'Female'];

  bool isSwitched = false;

  @override
  Widget build(BuildContext context) {
    return Directionality(
        textDirection: TextDirection.rtl,
        child: Scaffold(
            appBar: AppBar(
              elevation: 0,
              title: Center(
                child: Text(
                  "الاعدادات",
                  style: TextStyle(color: Colors.black),
                ),
              ),
              backgroundColor: anWhite,
              leading: InkWell(
                child: Icon(
                  Icons.arrow_back,
                  color: Colors.black,
                ),
                onTap: () {
                  Navigator.of(context).pop();
                },
              ),
            ),
            body: ListView(
              children: <Widget>[
                Container(
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.all(10),
                    color: anWhite,
                    height: MediaQuery
                        .of(context)
                        .size
                        .height / 4 + 50,
                    child: Column(
                      children: <Widget>[
                        CircleAvatar(
                          radius: 50,
                          backgroundColor: anRed,
                          child: Image.asset(
                            "assets/images/user.png",
                            scale: 5,
                          ),
                        ),
                        Text(nameCON.text ?? '...'),
                        Text(phoneCON.text ?? '...')
                      ],
                    )),
                SizedBox(
                  height: 20,
                ),
                Column(
                  children: <Widget>[
                    info(
                        TextField(
                          controller: nameCON,
                          decoration: InputDecoration(hintText: 'الإسم كامل'),
                          enabled: isSwitched,
                        ),
                        Icon(Icons.person)),
                    info(
                        TextField(
                          controller: phoneCON,
                          decoration: InputDecoration(hintText: 'رقم الهانف'),
                          enabled: isSwitched,
                        ),
                        Icon(Icons.phonelink_ring)),
                    info(
                        TextField(
                          controller: emailCON,
                          decoration:
                          InputDecoration(hintText: 'البريد الإلكتروني'),
                          enabled: false,
                        ),
                        Icon(Icons.email)),
                    info(
                        TextField(
                          controller: passwordCON,
                          decoration: InputDecoration(hintText: 'كلمة السر'),
                          enabled: isSwitched,
                        ),
                        Icon(Icons.lock)),
//                    FormField<String>(
//                      builder: (FormFieldState<String> state) {
//                        return InputDecorator(
//                          decoration: InputDecoration(
//                              border: OutlineInputBorder(
//                                  borderRadius: BorderRadius.circular(5.0))),
//                          child: DropdownButtonHideUnderline(
//                            child: DropdownButton<String>(
//                              hint: Text('النوع'),
//                              value: currentGValue ?? 'Male',
//                              isDense: true,
//                              onChanged: (newValue) {
//                                setState(() {
//                                  if (isSwitched) {
//                                    currentGValue = newValue;
//                                  }
//                                });
//                                print(currentGValue);
//                              },
//                              items: gTypes.map((String value) {
//                                return DropdownMenuItem<String>(
//                                  value: value,
//                                  child: Text(value),
//                                );
//                              }).toList(),
//                            ),
//                          ),
//                        );
//                      },
//                    ),
                    FlatButton(
                      child:
                      Text(isSwitched == false ? 'تعديل البيانات' : 'حفظ'),
                      onPressed: () {
                        if (isSwitched == true) {
                          updateAPI(
                              name: nameCON.text,
                              dId: id,
                              token: token,
                              passWord: passwordCON.text,
                              phone: phoneCON.text,
                            type: type
                              ).then((_) =>
                              Navigator.pushReplacement(context,
                                  MaterialPageRoute(builder: (context) =>
                                      HomeOnePage())));
                        }
                        setState(() {
                          isSwitched = !isSwitched;
                        });
                      },
                      textColor: anRed,
                      color: anWhite,
                    ),
                  ],
                ),
              ],
            )));
  }

  Widget info(Widget title, Icon icon) {
    return Container(
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(width: 1.0, color: anGrey),
          ),
          color: anWhite,
        ),
        child: ListTile(
          title: title,
          leading: icon,
        ));
  }

  @override
  void initState() {
      getData().then((userData) {
        setState(() {
          id = PrefManager.currentUser.id;
          token = PrefManager.currentUser.device_token;
          type = PrefManager.currentUser.type;
          phoneCON.text = userData[1];
          nameCON.text = userData[2];
          currentGValue = userData[3];
          emailCON.text = userData[4];
        });
    });
  }
}