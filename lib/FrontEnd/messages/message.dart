import 'package:jaki_kapten/services/exports.dart';
import 'package:intl/intl.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class MessagesPage extends StatefulWidget {
  final String title, id, notiId, type;

  const MessagesPage({Key key, this.title, this.id, this.notiId, this.type})
      : super(key: key);
  @override
  _AccountTabsPageState createState() => _AccountTabsPageState();
}

class _AccountTabsPageState extends State<MessagesPage> {
  List<String> titles = [];
  List<String> msgs = [];
  final TextEditingController _textController = new TextEditingController();
  final TextEditingController textReportController = new TextEditingController();
  static DateTime now = DateTime.now();
  String formattedDate = DateFormat('kk:mm:ss EEE d MMM').format(now);
  String UserName, UserPhone , drivId, token;

  final db = FirebaseFirestore.instance;
  final _formKey = GlobalKey<FormState>();
  final prokey = GlobalKey<FormState>();

  final validCharacters = RegExp(r'^[ ]');

  @override
  void initState() {

    super.initState();
    onGetData();
  }

  onGetData() async{
    getData().then((userData) async {
        var driverId = await PrefManager.getDriverId();

        drivId = driverId;
        UserName = userData[0];
        token = PrefManager.currentUser.device_token;
        UserPhone = userData[1];
        setState(() {

        });
        print("driver id${drivId}");
        print("user id${widget.id}");
    });

  }
  Future sendNotification(String token) async {
    String msg = _textController.text;
    final response = await Messaging.sendTo(
      title: "Jaki Kapten",
      body: "msg",
      fcmToken: token,
    );
    if (response.statusCode != 200) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content:
        Text('[${response.statusCode}] Error message: ${response.body}'),
      ));
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Center(
            child: Text(widget.title??"", style: TextStyle(color: Colors.black))),
        leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.arrow_back_ios,
              color: Colors.black,
            )),
        actions: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              InkWell(
                  onTap: () {
                    showDialog(
                        context: context,
                        builder: (context) => AlertDialog(
                          title: Text('للتبليغ عن عميل'),
                          actions: <Widget>[
                            Container(
                              child: TextField(
                                controller: textReportController,
                                onSubmitted: (value) {},
                                maxLines: 5,
                                decoration: InputDecoration(
                                    hintText: 'تفاصيل البلاغ'),
                              ),
                              height: 150,
                              width: 200,
                            ),
                            new FlatButton(
                              child: Text('توثيق'),
                              onPressed: () {
                                if(textReportController.text.trim()!=''){
                                  reportUser(widget.id,drivId,textReportController.text,token,widget.type).then((value){
                                    tostDialog("${widget.title} تم التبليغ عن ");
                                  });
                                }else{
                                  tostDialog("يجب ادخال تفاصيل البلاغ");
                                }
                              },
                            ),
                          ],
                        ));
                  },
                  child: Icon(
                    Icons.report,
                    color: Colors.black,
                  )),
            ],
          ),
        ],
        backgroundColor: anWhite,
      ),
      body: Column(
        children: <Widget>[
          new Flexible(
            child: StreamBuilder<QuerySnapshot>(
                stream: db
                    .collection('privatechat')
                    .orderBy('timestamp', descending: true)
                    .snapshots(),
                builder: (context, snapshot) {
                 // print(snapshot.data.docs);
                  if (snapshot.hasData) {
                    return  ListView(
                        padding: new EdgeInsets.all(8.0),
                        reverse: true,
                        children: snapshot.data.docs
                            .where((l) =>
                        l.data()['senderid'] ==drivId &&
                            l.data()['recevierid'] == widget.id ||
                            l.data()['recevierid'] == drivId &&
                                l.data()['senderid'] == widget.id)
                            .map((doc) {
                          return Container(
                            margin: const EdgeInsets.symmetric(vertical: 10.0),
                            child:  Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(right: 8.0),
                                    child: new CircleAvatar(
                                      child: Text(doc.data()['sendername'][0]),
                                    ),
                                  ),
                                  new Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: <Widget>[
                                      new Text(
                                        '${doc.data()['sendername']}',
                                        style: TextStyle(
                                            color: Colors.green,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      new Container(
                                        width: 150,
                                        margin: const EdgeInsets.only(top: 5.0),
                                        child: new Text(
                                          '${doc.data()['msg']}',
                                          maxLines: 15,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      )
                                    ],
                                  )
                                ]),
                          );
                        }).toList());
                  }
                  else if (ConnectionState.done != null) {
                    return SizedBox(
                      child: CircularProgressIndicator(
                        key: prokey,
                        backgroundColor: Colors.white,
                      ),
                    );
                  }
                  else {
                    return Center(
                      child: Container(
                          child: Text('No Messages yet, be First Sender')),
                    );
                  }
                }),
          ),
          new Divider(
            height: 1.0,
          ),
          new Container(
            decoration: new BoxDecoration(
              color: Theme.of(context).cardColor,
            ),
            child: _textComposerWidget(),
          ),
        ],
      ),
    );
  }

  void createData() async {
    if (_formKey.currentState.validate() == true &&
        _textController.text.isNotEmpty) {
      _formKey.currentState.save();
      await db.collection('privatechat').add({
        'msg': '${_textController.text}',
        'senderid': drivId,
        'sendername': UserName,
        'recevierid': widget.id,
        'receviername': widget.title,
        'notiId': widget.notiId??'',
        'timestamp': DateTime.now().millisecondsSinceEpoch.toString()
      });
    }
  }

  void _handleSubmitted(String text) {
    sendNotification(widget.notiId??'');
    createData();
    _textController.clear();
  }

  Widget _textComposerWidget() {
    return new IconTheme(
      data: new IconThemeData(color: Colors.blue),
      child: new Container(
        margin: const EdgeInsets.symmetric(horizontal: 8.0),
        child: new Row(
          children: <Widget>[
            new Flexible(
              child: Form(
                key: _formKey,
                child: new TextFormField(
                  decoration:
                  new InputDecoration.collapsed(hintText: "Send a message"),
                  controller: _textController,
                  onFieldSubmitted: _handleSubmitted,
                  validator: (value) {
                    if (validCharacters.hasMatch(value)) {
                      return 'edit your message';
                    } else {
                      return null;
                    }
                  },
                ),
              ),
            ),
            new Container(
              margin: const EdgeInsets.symmetric(horizontal: 4.0),
              child: new IconButton(
                icon: new Icon(Icons.send),
                onPressed: () => _handleSubmitted(_textController.text),
              ),
            )
          ],
        ),
      ),
    );
  }
}


class Messaging {
  static final Client client = Client();

  // from 'https://console.firebase.google.com'
  // --> project settings --> cloud messaging --> "Server key"
  static const String serverKey = "AAAAcKsL8pQ:APA91bEHJLB_T9b6i6e4fbpMY7teOye3jieNevDV2-vkMi6FeLzC9Z41PWRtM8UK5Sf1ZQP_NtNeda0jMPQ2z5tfR_NF5d-moqVCGKw1srtKlSY5V-44A04QA7Tyf40oT2Gk23vU6jCa";
  static Future<Response> sendTo({
    @required String title,
    @required String body,
    @required String fcmToken,
  }) =>
      client.post(
        'https://fcm.googleapis.com/fcm/send',
        body: json.encode({
          'notification': {'body': '$body', 'title': '$title'},
          'priority': 'high',
          'data': {
            'click_action': 'FLUTTER_NOTIFICATION_CLICK',
            'id': '1',
            'status': 'done',
          },
          'to': '$fcmToken',
        }),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'key=$serverKey',
        },
      );
}
