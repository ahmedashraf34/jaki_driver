import 'package:jaki_kapten/services/exports.dart';

class NotificationsPAge extends StatefulWidget {
  @override
  _NotificationsPAgeState createState() => _NotificationsPAgeState();
}

class _NotificationsPAgeState extends State<NotificationsPAge> {
  var data;

  final FirebaseMessaging _fcm = FirebaseMessaging();
  StreamSubscription iosSubscription;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Center(
              child: Text(
            "التنبيهات ",
            style: TextStyle(color: Colors.black),
          )),
          backgroundColor: anWhite,
          leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.arrow_back_ios,
              color: Colors.black,
            ),
          ),
        ),
        body: Directionality(
            textDirection: TextDirection.rtl,
            child: data == null ? Center(child: Text('لا توجد إشعارات مثبتة لديك')) : ListView.builder(
              itemCount: data.length,
              itemBuilder: (context, index) {
                return cards(data[index]['drop_area'], data[index]['drop_area']);
              },
            )));
  }

  Widget cards(name, title) {
    return Card(
      child: ListTile(
        title: Text("$name"),
        subtitle: Text("$title"),
        leading: CircleAvatar(
          child: Text(
            "${name[0]}",
            style: TextStyle(fontSize: 23),
          ),
          radius: 40,
          backgroundColor: anGrey,
        ),
      ),
    );
  }


  @override
  void initState() {
    super.initState();
    if (Platform.isIOS) {
      iosSubscription = _fcm.onIosSettingsRegistered.listen((data) {
        // save the token  OR subscribe to a topic here
      });

      _fcm.requestNotificationPermissions(IosNotificationSettings());
    }
    _fcm.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            content: ListTile(
              title: Text(message['notification']['title']),
              subtitle: Text(message['notification']['body']),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () => Navigator.of(context).pop(),
              ),
            ],
          ),
        );
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        // TODO optional
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        // TODO optional
      },
    );

  }
//  @override
//  void initState() {
//    getToken().then((userInput) {
//      getUserLocations(userInput[1], userInput[0]).then((value) {
//        setState((){
//          data = value;
//          print(data);
//        });
//      });
//    });
//
//    super.initState();
//  }
}
