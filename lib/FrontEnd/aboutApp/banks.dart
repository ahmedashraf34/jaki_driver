
import 'package:jaki_kapten/services/exports.dart';

class BanksPage extends StatefulWidget {
  @override
  BanksPageState createState() => BanksPageState();
}

class BanksPageState extends State<BanksPage> {
  bool isSwitched = false;
  var data;
  @override
  Widget build(BuildContext context) {
    return Directionality(
        textDirection: TextDirection.rtl,
        child: Scaffold(
            body: data!=null?ListView.builder(
              itemCount: data.length,
              itemBuilder: (context, index) {
                return Card(
                  child: Text('${data[index]['BankName']} حساب رقم : ${data[index]['AccountID']}'),
                  elevation: 5,
                );
              },
            ):CircularProgressIndicator()));
  }


  @override
  void initState() {
    banksAPI().then((value) {
      setState(() {
        data = value;
      });
    });

    super.initState();
  }
}
