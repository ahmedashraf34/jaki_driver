import 'package:jaki_kapten/services/exports.dart';


class TermesPage extends StatefulWidget {
  final String content;

  const TermesPage({Key key, this.content}) : super(key: key);

  @override
  _PaymentPageState createState() => _PaymentPageState();
}

class _PaymentPageState extends State<TermesPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: anWhite,
        body: Container(
            padding: EdgeInsets.all(10),
            child: Center(
                child: SingleChildScrollView(
                    child: Text(widget.content, maxLines: 200,))
            )
        )
    );
  }
}