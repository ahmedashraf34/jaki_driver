import 'package:jaki_kapten/services/exports.dart';


class PlacesCat extends StatefulWidget {
  @override
  PlacesCatState createState() => PlacesCatState();
}

class PlacesCatState extends State<PlacesCat> {
  bool isSwitched = false;
  var data;
  @override
  Widget build(BuildContext context) {
    return Directionality(
        textDirection: TextDirection.rtl,
        child: Scaffold(
            body: data!=null?ListView.builder(
          itemCount: data.length,
          itemBuilder: (context, index) {
            return Card(
              child: Text(data[index]['title']),
              elevation: 5,
            );
          },
        ):Center(child: CircularProgressIndicator())
        ));
  }


  @override
  void initState() {
    placescatsAPI().then((value) {
      setState(() {
        data = value;
      });
    });

    super.initState();
  }
}
