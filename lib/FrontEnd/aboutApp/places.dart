import 'package:jaki_kapten/services/exports.dart';


class PlacesPage extends StatefulWidget {
  @override
  PlacesPageState createState() => PlacesPageState();
}

class PlacesPageState extends State<PlacesPage> {
  bool isSwitched = false;
  var data;
  @override
  Widget build(BuildContext context) {
    return Directionality(
        textDirection: TextDirection.rtl,
        child: Scaffold(
            body: data!=null?ListView.builder(
          itemCount: data.length,
          itemBuilder: (context, index) {
            return Card(
              child: Text(data[index]['name']),
              elevation: 5,
            );
          },
        ):Center(child: CircularProgressIndicator())
        ));
  }


  @override
  void initState() {
    placesAPI().then((value) {
      setState(() {
        data = value;
      });
    });

    super.initState();
  }
}
