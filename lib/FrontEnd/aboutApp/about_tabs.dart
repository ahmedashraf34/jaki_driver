import 'package:jaki_kapten/FrontEnd/aboutApp/about.dart';
import 'package:jaki_kapten/FrontEnd/aboutApp/banks.dart';
import 'package:jaki_kapten/FrontEnd/aboutApp/places.dart';
import 'package:jaki_kapten/FrontEnd/aboutApp/placesCat.dart';
import 'package:jaki_kapten/FrontEnd/aboutApp/terms.dart';
import 'package:jaki_kapten/services/exports.dart';


class SettingTabsPage extends StatefulWidget {
  final int value;

  const SettingTabsPage({Key key, this.value}) : super(key: key);
  @override
  SettingTabsPageState createState() => SettingTabsPageState();
}

class SettingTabsPageState extends State<SettingTabsPage> {
  String title,content;

  @override
  Widget build(BuildContext context) {
    return Directionality(
        textDirection: TextDirection.rtl,
        child: title != null?DefaultTabController(
          length: 5,
          initialIndex: widget.value,
          child: Scaffold(
            appBar: AppBar(
              backgroundColor: anWhite,
              leading: InkWell(
                child: Icon(
                  Icons.arrow_back,
                  color: Colors.black,
                ),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              bottom: TabBar(
                labelColor: anRed,
                isScrollable: true,
                unselectedLabelColor: Colors.black,
                tabs: [
                  Tab(
                    text: 'نحن',
                  ),
                  Tab(
                    text: title,
                  ),
                  Tab(
                    text: 'الأماكن',
                  ),
                  Tab(
                    text: 'فئات الأماكن',
                  ),
                  Tab(
                    text: 'البنوك والمصارف',
                  ),
                ],
              ),
              title: Container(
                  margin: EdgeInsets.only(top: 15),
                  child: Center(
                      child: Text('عن التطبيق', style: TextStyle(color: Colors.black),))),
            ),
            body: TabBarView(
              children: [
                AboutPage(),
                TermesPage(content: content),
                PlacesPage(),
                PlacesCat(),
                BanksPage(),
              ],
            ),
          ),
        ):Center(child:CircularProgressIndicator()));
  }

  @override
  void initState() {
    termsAPI().then((value) {
      setState(() {
        title = value['PageName'];
        content = value['PageContent'];
      });
    });
    super.initState();
  }
}

