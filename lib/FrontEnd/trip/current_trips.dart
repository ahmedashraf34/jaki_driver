import 'package:jaki_kapten/services/exports.dart';

class CurrentTripsPage extends StatefulWidget {
  @override
  _CurrentTripsPageState createState() => _CurrentTripsPageState();
}

class _CurrentTripsPageState extends State<CurrentTripsPage> {
  var data, drivId, token, type;

  @override
  void initState() {
    getData();
    super.initState();
  }

  getData() {
      tripsDetails().then((value) {
        setState(() {
          drivId = PrefManager.currentUser.id;
          token = PrefManager.currentUser.device_token;
          type = PrefManager.currentUser.type;
          data = value;
          print("daaaaaaaaaata${data}");
        });
      });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        color: anGrey,
        child: data == null
            ? Center(child: CircularProgressIndicator())
            : data.length == 0
                ? Center(child: Text("لا توجد رحلات حالية لك"))
                : ListView.builder(
                    itemCount: data.length,
                    itemBuilder: (context, index) {
                      return tripList(
                          ontap: () {
                            showDialog(
                                context: context,
                                builder: (context) => AlertDialog(
                                      title: Row(
                                        children: <Widget>[
                                          new Text(
                                            'حول الرحلة',
                                            textDirection: TextDirection.ltr,
                                            textAlign: TextAlign.center,
                                          ),
                                          new IconButton(
                                            icon: Icon(
                                              Icons.chat,
                                              color: Colors.black,
                                            ),
                                            onPressed: () {
                                              print(data[index]['user_id	']);
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          MessagesPage(
                                                            title: data[index]
                                                                ['username'],
                                                            id: data[index]
                                                                ['user_id	'],
                                                            notiId: data[index][
                                                                    'DeviceID'] ??
                                                                '',
                                                          )));
                                            },
                                          )
                                        ],
                                      ),
                                      content: new Text(
                                        '''حالة الرحلة
                                    ${data[index]['status_code'].toString().replaceAll('-', ' ')}''',
                                        textDirection: TextDirection.ltr,
                                        textAlign: TextAlign.end,
                                      ),
                                      actions: <Widget>[
                                        data[index]['status'] == 1 ||
                                            data[index]['status'] == 3 ||
                                            data[index]['status'] == '1' ||
                                            data[index]['status'] == '3'
                                            ? new FlatButton(
                                          child: new Text(
                                            "إكمال الرحلة",
                                            textDirection: TextDirection.ltr,
                                            textAlign: TextAlign.end,
                                          ),
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                            driverEndTrip(
                                                    drivId,
                                                    token,
                                                    data[index]['id'],
                                                    data[index]['drop_lat'],
                                                    data[index]['drop_longs'],
                                                    type)
                                                .then((value) {
                                              getData();
                                              tostDialog(
                                                  "تم إكمال الرحلة بنجاح");
                                            });
                                          },
                                        ):Container(),
                                        data[index]['status'] == 1 ||
                                                data[index]['status'] == 3 ||
                                                data[index]['status'] == '1' ||
                                                data[index]['status'] == '3'
                                            ? new FlatButton(
                                                child: new Text("أبدأ الرحلة"),
                                                onPressed: () {
                                                  Navigator.of(context).pop();
                                                  driverStartTrip(
                                                          drivId,
                                                          token,
                                                          data[index]['id'],
                                                          type)
                                                      .then((value) {
                                                    getData();
                                                    tostDialog("تم بدأ الرحلة");
                                                  });
                                                },
                                              )
                                            : Container(),
                                        new FlatButton(
                                          child: Text("إلغاء الرحلة"),
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                            driverCancelTrip(drivId, token,
                                                    data[index]['id'], type)
                                                .then((value) {
                                              getData();
                                              tostDialog("تم إلغاء الرحلة");
                                            });
                                          },
                                        ),
                                        new FlatButton(
                                          child: Text("عرض"),
                                          onPressed: () {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          HomeOnePage(
                                                            recLatPick: double
                                                                .parse(data[
                                                                        index][
                                                                    'pickup_lat']),
                                                            recLngPick: double
                                                                .parse(data[
                                                                        index][
                                                                    'pickup_longs']),
                                                            recLatdrop: double
                                                                .parse(data[
                                                                        index][
                                                                    'drop_lat']),
                                                            recLngdrop: double
                                                                .parse(data[
                                                                        index][
                                                                    'drop_longs']),
                                                            tripId: data[index]
                                                                ['id'],
                                                          )));

                                          },
                                        ),
                                      ],
                                    ));
                          },
                          date: data[index]['book_create_date_time'] ?? 'NUll',
                          amount: data[index]['amount'],
                          from: data[index]['pickup_area'],
                          to: data[index]['drop_area']);
                    },
                  ));
  }
}
/*
8
on-trip

7
driver-arrived

9
completed

5
driver-cancelled

4
user-cancelled

1,3
pending

11
admin-completed
 */

/*
[{id: 270, pickup_area: Giza Governorate, drop_area: بورسعيد, book_create_date_time: 0000-00-00 00:00:00, pickup_lat: 29.9416665, pickup_longs: 30.9806413, drop_lat: 31.2652893, drop_longs: 32.3018661, approx_time: , amount: 0, status: 3, status_code: Driver-accepted, user_id	: 79, username: mohamed_asss, DeviceID: ebh_6o_ERdCuudI4tCMrRm:APA91bF3z29scje4qCVuu3w6IU8ROITs9Tc8kFpDKpyQN4KIquva2SwGX3nUHsvcNgefICkHmDjt9eRLMESXgtPqL5kovQR3suqVdbP2ct9OHrVFlAkcHkLGX4DWoaGVzjg_3AS93ruI, userMobile: 01142174778}]
 */
