import 'package:jaki_kapten/services/exports.dart';

class HoistryTripPage extends StatefulWidget {
  @override
  _HoistryTripPageState createState() => _HoistryTripPageState();
}

class _HoistryTripPageState extends State<HoistryTripPage> {
  var data, userId , token , type;
  @override
  void initState() {
      getUserLocationsCompleted().then((value) {
        setState(() {
          data = value;
          token = PrefManager.currentUser.device_token;
          type = PrefManager.currentUser.type;
          userId = PrefManager.currentUser.id;
        });
      });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        color: anGrey,
        child: data == null || data == ['']
            ? Center(child: CircularProgressIndicator())
            : data.length == 0
                ? Center(child: Text("لا توجد رحلات مكتملة لك"))
                : ListView.builder(
                    itemCount: data.length,
                    itemBuilder: (context, index) {
                      return tripList(
                          ontap: () {
                            getTripPrice(data[index]['id'],userId,token,type).then((value) {
                                  tostDialog('final amount : $value SAR');
                                });
                          },
                          date: data[index]['book_create_date_time'],
                          from: data[index]['pickup_area'],
                          to: data[index]['drop_area']);
                    },
                  ));
  }
}
