import 'package:jaki_kapten/services/Models/trip_model.dart';
import 'package:jaki_kapten/services/exports.dart';

class NextTripPage extends StatefulWidget {
  @override
  _NextTripPageState createState() => _NextTripPageState();
}

class _NextTripPageState extends State<NextTripPage> {
  List<TripModel> pendingTrips;
  String drivId, token, type;

  TextEditingController priceControler = new TextEditingController();
  @override
  void initState() {
    getUserPendingTrips().then((value) {
        setState(() {
          drivId = PrefManager.currentUser.id;
          token = PrefManager.currentUser.device_token;
          type = PrefManager.currentUser.type;
          pendingTrips = value;
        });
        print(type);
      });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        color: anGrey,
        child: pendingTrips == null
            ? Center(child: CircularProgressIndicator())
            : pendingTrips.isEmpty
                ? Center(child: Text("لا توجد رحلات معلقة"))
                : ListView.builder(
                    itemCount: pendingTrips.length,
                    itemBuilder: (context, index) {
                      return tripList(
                          ontap: () async{
                            await showDialog(
                                context: context,
                                builder: (context) => AlertDialog(
                                      title:
                                          new Text('إشعار موافقة على الرحلة'),
                                      content: type != 'Driver'
                                          ? TextField(controller: priceControler,decoration: InputDecoration(hintText: 'أدخل السعر لهذا الطلب'),)
                                          : new Text('هل توافق على الرحلة؟'),
                                      actions: <Widget>[
                                        new FlatButton(
                                          child: new Text("موافقة"),
                                          onPressed: () async{
                                            if (type != 'Driver') {
                                              nondriverAcceptTrip(drivId, token,
                                                  pendingTrips[index].id,priceControler.text,type)
                                                  .then((value) {
                                                tostDialog(
                                                    "تم ارسال السعر المطلوب وننتظر موافقة العميل");
                                                Navigator.of(context).pop();

                                              });
                                            }else {
                                              await driverAcceptTrip(drivId, token, pendingTrips[index].id)
                                                  .then((value) {
                                                Navigator.of(context).pop();
                                                tostDialog(value[1]);
                                                    if(value[0]) {
                                                      Navigator.push(
                                                          context,
                                                          MaterialPageRoute(
                                                              builder: (
                                                                  context) =>
                                                                  HomeOnePage(
                                                                    recLatPick: double
                                                                        .parse(
                                                                        pendingTrips[
                                                                        index]
                                                                            .pickLatitude),
                                                                    recLngPick: double
                                                                        .parse(
                                                                        pendingTrips[
                                                                        index]
                                                                            .pickLongitude),
                                                                    recLatdrop: double
                                                                        .parse(
                                                                        pendingTrips[
                                                                        index]
                                                                            .dropLatitude),
                                                                    recLngdrop: double
                                                                        .parse(
                                                                        pendingTrips[
                                                                        index]
                                                                            .dropLongitude),
                                                                    tripId:
                                                                    pendingTrips[index]
                                                                        .id,
                                                                  )
                                                          )
                                                      );
                                                    }
                                              });
                                            }
                                          },
                                        ),
                                        new FlatButton(
                                          child: Text("إلغاء"),
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                        ),
                                      ],
                                    ));
                          },
                          date: pendingTrips[index].bookCreateDateTime,
                          from: pendingTrips[index].pickArea,
                          to: pendingTrips[index].dropArea);
                    },
                  ));
  }
}

