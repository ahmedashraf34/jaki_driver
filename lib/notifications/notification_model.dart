class ReceivedNotification {
  final int id;
  final String title;
  final String body;
  final String date;
  final String payload;

  ReceivedNotification({
    this.id,
    this.title,
    this.body,
    this.date,
    this.payload,
  });
}