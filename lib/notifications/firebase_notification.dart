import 'dart:convert';
import 'dart:io';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:jaki_kapten/services/pref.dart';

import 'local_notifications.dart';

class FirebaseNotifications {
  static FirebaseMessaging firebaseMessaging = FirebaseMessaging();
  static bool _initialized = false;

  static void _iOSPermission() {
    firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true)
    );
    firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }


  static Future<void> init() async {
    onShowModelSheet(String text, String title,
        {String productId, bool click = false}) {
      return BotToast.showNotification(
        enableSlideOff: true,
        onlyOne: true,
        subtitle: (_) => Text(
          text,
          style: TextStyle(
            fontFamily: "cairo",
          ),
        ),
//      onClose: (){
//        if(pop){
//          Navigator.pop(context);
//        }
//      },
        onTap: () {
          if (click) {}
        },
        crossPage: true,
        leading: (_) => Icon(
          Icons.notifications_active,
          color: Colors.blue,
          size: 30,
        ),
        animationDuration: Duration(milliseconds: 500),
        animationReverseDuration: Duration(milliseconds: 500),
        duration: Duration(seconds: 5),
        title: (_) => Text(
          title,
          style: TextStyle(
            fontFamily: "cairo",
          ),
        ),
      );
    }
    if (!_initialized) {
      if (Platform.isIOS) _iOSPermission();

      firebaseMessaging.configure(
        //onBackgroundMessage: Platform.isAndroid ?myBackgroundMessageHandler:null,
        onMessage: (Map<String, dynamic> message) async {
          print("onMessage: $message");
          onShowModelSheet(message['notification']['body'], message['notification']['title']);

        },
        onLaunch: (Map<String, dynamic> message) async {
          print("onLaunch: $message");
          LocalNotifications.show(title: message['notification']['title'],
              body: message['notification']['body']);
        },
        onResume: (Map<String, dynamic> message) async {
          print("onResume: $message");
          LocalNotifications.show(title: message['notification']['title'],
              body: message['notification']['body']);
        },
      );
      _initialized = true;
    }
  }

  static Future<String> getToken() async {
    print("reaaaaaaaaaaaaaaaaaaaaaaach");
    // For testing purposes print the Firebase Messaging token
    String token = await firebaseMessaging.getToken();
    print("FirebaseMessaging token: $token");
    return token;
  }


  // Replace with server token from firebase console settings.
  final String serverToken = 'AAAAcKsL8pQ:APA91bEHJLB_T9b6i6e4fbpMY7teOye3jieNevDV2-vkMi6FeLzC9Z41PWRtM8UK5Sf1ZQP_NtNeda0jMPQ2z5tfR_NF5d-moqVCGKw1srtKlSY5V-44A04QA7Tyf40oT2Gk23vU6jCa';

  Future<void> sendNotification(String firebaseToken,
      {String body, String title}) async {
    print("NOTIFICATION SENT!");
    await http.post(
      'https://fcm.googleapis.com/fcm/send',
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Authorization': 'key=$serverToken',
      },
      body: jsonEncode(
        <String, dynamic>{
          'notification': <String, dynamic>{
            'body': body.isEmpty ? null : body,
            'title': title.isEmpty ? null : title
          },
          'priority': 'high',
          'data': <String, dynamic>{
            'click_action': 'FLUTTER_NOTIFICATION_CLICK',
            'id': '1',
            'status': 'done'
          },
          'to': firebaseToken,
        },
      ),
    );
  }

}