class PrefKeys {
  static const String XXX = "";

  static const String FIRST_TIME = "firstTime";

  static const String USER_TOKEN = "userToken";
  static const String USER_STATUS = "userStatus";
  static const String USER_CODE = "userCode";
  static const String USER_ID = "userID";
  static const String USER_PASSWORD = "userPassword";
  static const String USER_NAME = "userName";
  static const String USER_PHONE = "userPhone";
  static const String USER_EMAIL = "userEmail";
  static const String USER_GENDER = "userGender";
  static const String USER_IMAGE = "userImage";


  static const String USER_TYPE = "userType";
  static const String USER_FULL_NAME = "userFullName";
  static const String USER_SUR_NAME = "userSurName";
  static const String USER_ADDRESS = "userAddress";
  static const String USER_CAR_IMAGE = "userCarImage";
  static const String USER_CAR_NUMBER = "userCarNumber";
  static const String USER_CAR_TYPE = "userCarType";
  static const String USER_CAR_SEATING_CAPACITY = "userCarSeatingCapacity";
}
