

import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:jaki_kapten/services/Models/UserData.dart';

import 'pref_keys.dart';
import 'pref_util.dart';

class PrefManager {
  static UserData currentUser = UserData();

  static Future<void> clearUserData()async{
    await setUserID("");
    await setUserName("");
    await setUserToken("");
    await setUserPhone("");
    await setUserEmail("");
    await setUserGender("");
    await setUserImageURL("");
    await setUserCode("");
    await setUserStatus("");
    await setUserType("");
    await setUserFullName("");
    await setUserSurName("");
    await setUserAddress("");
    await setUserCarImage("");
    await setUserCarNumber("");
    await setUserCarType("");
    await setUserCarSeatingCapacity("");

  }

  static Future<void> initializePrefData() async {
    currentUser.phone = await getUserPhone();
    currentUser.password = await getUserPassword();
    currentUser.id = await getUserID();
    currentUser.device_token = await getUserToken();
    currentUser.name = await getUserName();
    currentUser.email = await getUserEmail();
    currentUser.gender = await getUserGender();
    currentUser.image = await getUserImageURL();
    currentUser.userCode = await getUserCode();
    currentUser.status = await getUserStatus();
    currentUser.type = await getUserType();
    currentUser.address = await getUserAddress();
    currentUser.user_name = await getUserFullName();
    currentUser.surname = await getUserSurName();
    currentUser.car_image = await getUserCarImage();
    currentUser.car_no = await getUserCarNumber();
    currentUser.car_type = await getUserCarType();
    currentUser.Seating_Capacity = await getUserCarSeatingCapacity();
  }

  static Future<void> setPrefData(UserData userModel) async {
    await setUserName(userModel.name);
    await setUserFullName(userModel.user_name);
    await setUserSurName(userModel.surname);
    await setUserEmail(userModel.email);
    await setUserGender(userModel.gender);
    await setUserToken(userModel.device_token);
    await setUserPhone(userModel.phone);
    await setUserID(userModel.id);
    await setUserCode(userModel.userCode);
    await setUserStatus(userModel.status);
    await setUserImageURL(userModel.image);
    await setUserPassword(userModel.password);
    //await setUserType(userModel.type);
    await setUserAddress(userModel.address);
    await setUserCarImage(userModel.car_image);
    await setUserCarNumber(userModel.car_no);
    await setUserCarType(userModel.car_type);
    await setUserCarSeatingCapacity(userModel.Seating_Capacity);
  }

  //
  static Future<void> setUserSurName(String surName) async {
    currentUser.surname = surName;
    await PrefUtils.setString(PrefKeys.USER_SUR_NAME, surName);
  }

  static Future<String> getUserSurName() async {
    return await PrefUtils.getString(PrefKeys.USER_SUR_NAME);
  }

  static Future<void> setUserFullName(String fullName) async {
    currentUser.user_name = fullName;
    await PrefUtils.setString(PrefKeys.USER_FULL_NAME, fullName);
  }

  static Future<String> getUserFullName() async {
    return await PrefUtils.getString(PrefKeys.USER_FULL_NAME);
  }

  static Future<void> setUserType(String type) async {
    currentUser.type = type;
    await PrefUtils.setString(PrefKeys.USER_TYPE, type);
  }

  static Future<String> getUserType() async {
    return await PrefUtils.getString(PrefKeys.USER_TYPE);
  }

  static Future<void> setUserAddress(String address) async {
    currentUser.address = address;
    await PrefUtils.setString(PrefKeys.USER_ADDRESS, address);
  }

  static Future<String> getUserAddress() async {
    return await PrefUtils.getString(PrefKeys.USER_ADDRESS);
  }

  static Future<void> setUserCarImage(String carImage) async {
    currentUser.car_image = carImage;
    await PrefUtils.setString(PrefKeys.USER_CAR_IMAGE, carImage);
  }

  static Future<String> getUserCarImage() async {
    return await PrefUtils.getString(PrefKeys.USER_CAR_IMAGE);
  }

  static Future<void> setUserCarNumber(String carNumber) async {
    currentUser.car_no = carNumber;
    await PrefUtils.setString(PrefKeys.USER_CAR_NUMBER, carNumber);
  }

  static Future<String> getUserCarNumber() async {
    return await PrefUtils.getString(PrefKeys.USER_CAR_NUMBER);
  }

  static Future<void> setUserCarType(String carType) async {
    currentUser.car_type = carType;
    await PrefUtils.setString(PrefKeys.USER_CAR_TYPE, carType);
  }

  static Future<String> getUserCarType() async {
    return await PrefUtils.getString(PrefKeys.USER_CAR_TYPE);
  }

  static Future<void> setUserCarSeatingCapacity(String carSeatingCapacity) async {
    currentUser.Seating_Capacity = carSeatingCapacity;
    await PrefUtils.setString(PrefKeys.USER_CAR_SEATING_CAPACITY, carSeatingCapacity);
  }

  static Future<String> getUserCarSeatingCapacity() async {
    return await PrefUtils.getString(PrefKeys.USER_CAR_SEATING_CAPACITY);
  }

  //

  static Future<void> setUserToken(String token) async {
    currentUser.device_token = token;
    await PrefUtils.setString(PrefKeys.USER_TOKEN, token);
  }

  static Future<String> getUserToken() async {
    return await PrefUtils.getString(PrefKeys.USER_TOKEN);
  }

  static Future<void> setUserStatus(String status) async {
    currentUser.status = status;
    await PrefUtils.setString(PrefKeys.USER_STATUS, status);
  }

  static Future<String> getUserStatus() async {
    return await PrefUtils.getString(PrefKeys.USER_STATUS);
  }

  static Future<void> setUserCode(String code) async {
    currentUser.userCode = code;
    await PrefUtils.setString(PrefKeys.USER_CODE, code);
  }

  static Future<String> getUserCode() async {
    return await PrefUtils.getString(PrefKeys.USER_CODE);
  }

  static Future<void> setNotFirstTime(bool val) async {
    await PrefUtils.setBool(PrefKeys.FIRST_TIME, val);
  }

  static Future<bool> getNotFirstTime() async {
    return await PrefUtils.getBool(PrefKeys.FIRST_TIME);
  }

  static Future<void> setUserID(String id) async {
    currentUser.id = id;
    await PrefUtils.setString(PrefKeys.USER_ID, id);
  }

  static Future<String> getUserID() async {
    return await PrefUtils.getString(PrefKeys.USER_ID);
  }

  static Future<void> setUserPassword(String password) async {
    currentUser.password = password;
    await PrefUtils.setString(PrefKeys.USER_PASSWORD, password);
  }

  static Future<String> getUserPassword() async {
    return await PrefUtils.getString(PrefKeys.USER_PASSWORD);
  }

  static Future<void> setUserPhone(String phone) async {
    currentUser.phone = phone;
    await PrefUtils.setString(PrefKeys.USER_PHONE, phone);
  }

  static Future<String> getUserPhone() async {
    return await PrefUtils.getString(PrefKeys.USER_PHONE);
  }

  static Future<void> setUserName(String name) async {
    currentUser.name = name;
    await PrefUtils.setString(PrefKeys.USER_NAME, name);
  }

  static Future<String> getUserName() async {
    return await PrefUtils.getString(PrefKeys.USER_NAME);
  }

  static Future<void> setUserEmail(String email) async {
    currentUser.email = email;
    await PrefUtils.setString(PrefKeys.USER_EMAIL, email);
  }

  static Future<String> getUserEmail() async {
    return await PrefUtils.getString(PrefKeys.USER_EMAIL);
  }

  static Future<void> setUserGender(String gender) async {
    currentUser.gender = gender;
    await PrefUtils.setString(PrefKeys.USER_GENDER, gender);
  }

  static Future<String> getUserGender() async {
    return await PrefUtils.getString(PrefKeys.USER_GENDER);
  }

  static Future<void> setUserImageURL(String url) async {
    currentUser.image = url;
    await PrefUtils.setString(PrefKeys.USER_IMAGE, url);
  }

  static Future<String> getUserImageURL() async {
    return await PrefUtils.getString(PrefKeys.USER_IMAGE);
  }
  static Future<String> getFireBaseToken() async {
    return await PrefUtils.getString('firebaseToken' ?? '');
  }

  static Future<String> setFireBaseToken(String value) async {
    await PrefUtils.setString("firebaseToken", value);
  }

  static Future<String> getDriverId() async {
    return await PrefUtils.getString('driverIdd' ?? '');
  }

  static Future<String> setDriverId(String value) async {
    await PrefUtils.setString("driverIdd", value);
  }

  tostDialog(String message) {
    return Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Color(0xff222222),
        textColor: Colors.white,
        fontSize: 18.0);
  }
}
