
class TripModel{
  String id;
  String bookCreateDateTime, approximateTime, amount, status, distance;
  String pickArea, pickLatitude, pickLongitude;
  String dropArea, dropLatitude, dropLongitude;
  String userID, userName, userMobile;

  TripModel({
    this.id,
    this.bookCreateDateTime,
    this.approximateTime,
    this.amount,
    this.status,
    this.distance,

    this.pickArea,
    this.pickLatitude,
    this.pickLongitude,

    this.dropArea,
    this.dropLatitude,
    this.dropLongitude,

    this.userID,
    this.userName,
    this.userMobile
  });

  TripModel.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'].toString();
      bookCreateDateTime = jsonMap['book_create_date_time'] != null ? jsonMap['book_create_date_time'].toString() : null;
      approximateTime = jsonMap['approx_time'] != null ? jsonMap['approx_time'] : null;
      amount = jsonMap['amount'] != null ? jsonMap['amount'].toString() : null;
      status = jsonMap['status'] != null ? jsonMap['status'].toString() : null;
      distance = jsonMap['km'] != null ? jsonMap['km'].toString() : null;

      pickArea = jsonMap['pickup_area'] != null ? jsonMap['pickup_area'].toString() : null;
      pickLatitude = jsonMap['pickup_lat'] != null ? jsonMap['pickup_lat'].toString() : null;
      pickLongitude = jsonMap['pickup_longs'] != null ? jsonMap['pickup_longs'].toString() : null;

      dropArea = jsonMap['drop_area'] != null ? jsonMap['drop_area'].toString() : null;
      dropLatitude = jsonMap['drop_lat'] != null ? jsonMap['drop_lat'].toString() : null;
      dropLongitude = jsonMap['drop_longs'] != null ? jsonMap['drop_longs'].toString() : null;

      userID = jsonMap['user_id'] != null ? jsonMap['user_id'].toString() : null;
      userName = jsonMap['username'] != null ? jsonMap['username'].toString() : null;
      userMobile = jsonMap['userMobile'] != null ? jsonMap['userMobile'].toString() : null;

    } catch (e) {
      print("Trip Model: " + e.toString());
    }
  }

  toMap(){
    Map map = Map<String, dynamic>();
    map["id"] = id;
    map["book_create_date_time"] = bookCreateDateTime;
    map["approx_time"] = approximateTime;
    map["amount"] = amount;
    map["status"] = status;
    map["km"] = distance;

    map["pickup_area"] = pickArea;
    map["pickup_lat"] = pickLatitude;
    map["pickup_longs"] = pickLongitude;

    map["drop_area"] = dropArea;
    map["drop_lat"] = dropLatitude;
    map["drop_longs"] = dropLongitude;

    map["user_id"] = userID;
    map["username"] = userName;
    map["userMobile"] = userMobile;

    return map;
  }
}