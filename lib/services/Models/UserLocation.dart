class UserLocations {
  String id,
      pickup_area,
      drop_area,
      book_create_date_time,
      pickup_lat,
      pickup_longs,
      drop_lat,
      drop_longs,
      approx_time,
      amount,
      status,
      user_id,
      username,
      km,
      userMobile;

  UserLocations(
      {this.id,
      this.pickup_area,
      this.drop_area,
      this.book_create_date_time,
      this.pickup_lat,
      this.pickup_longs,
      this.drop_lat,
      this.drop_longs,
      this.approx_time,
      this.amount,
      this.status,
      this.user_id,
      this.username,
      this.km,
      this.userMobile});

  factory UserLocations.fromJson(Map<String, String> json) {
    return UserLocations(
        id: json['id'],
        pickup_area: json['pickup_area'],
        drop_area: json['drop_area'],
        book_create_date_time: json['book_create_date_time'],
        pickup_lat: json['pickup_lat'],
        pickup_longs: json['pickup_longs'],
        drop_lat: json['drop_lat'],
        drop_longs: json['drop_longs'],
        approx_time: json['approx_time'],
        amount: json['amount'],
        status: json['status'],
        user_id: json['user_id'],
        username: json['username'],
        km: json['km'],
        userMobile: json['userMobile']);
  }
}
