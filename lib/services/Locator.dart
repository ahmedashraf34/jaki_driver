import 'package:get_it/get_it.dart';
import 'package:rxdart/rxdart.dart';

import 'FireBaseSerivce.dart';

GetIt locator = GetIt.instance;

void setupLocator() async {

  locator.registerLazySingleton(() => FireBaseSerivce());



  
  locator.registerLazySingleton(() => PublishSubject<bool>());
  locator.registerLazySingleton(() => PublishSubject<String>());
  locator.registerLazySingleton(() => PublishSubject<Map>());
}
