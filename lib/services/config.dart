import 'package:dio/dio.dart' as doz;
import 'package:jaki_kapten/services/Models/UserData.dart';
import 'package:jaki_kapten/services/exports.dart';

import 'Models/trip_model.dart';

final String baseUrl =
    'https://jaki-taxi.com/admin/android/index.php/web_service';
final String driNUrl = 'https://jaki-taxi.com/admin/LatestApi/RestFul/Drivers';
final String driTUrl =
    'https://jaki-taxi.com/admin/LatestApi/RestFul/TransportationDrivers';
final String driWUrl =
    'https://jaki-taxi.com/admin/LatestApi/RestFul/TruckDrivers';
final String generalUrl =
    'http://jaki-taxi.com/admin/LatestApi/RestFul/General';

doz.Dio dio = doz.Dio()
  ..interceptors.add(
    doz.LogInterceptor(
      responseBody: true,
      error: true,
      requestBody: true,
      requestHeader: true,
    ),
  );

// Future<doz.Response> onSaveToken({var devId, var driverId, var token}) {
//   Map<String, dynamic> query = {
//     "DeviceID": devId,
//     "DriverID": driverId,
//     "token": token,
//   };
//
//   return dio.post(
//     'http://jaki-taxi.com/admin/LatestApi/RestFul/Drivers/UpdateDriverDeviceID.php?DeviceID=${devId}&DriverID${driverId}&token=${token}',
//   );
// }

onSaveToken({var devId, var driverId, var token, var servicesType}) async {
  try {
    var tokenUrl =
        'http://jaki-taxi.com/admin/LatestApi/RestFul/${servicesType}/UpdateDriverDeviceID.php?' +
            'DeviceID=$devId&' +
            'DriverID=$driverId&' +
            'token=$token';
    var request = await get(tokenUrl);
    var data = jsonDecode(request.body);
    print(tokenUrl);
    print(data);
  } catch (e) {
    print(e.toString());
    tostDialog("خطأ في تسجيل الدخول");
  }
}

onLogOut({var devId, var driverId, var token, var servicesType}) async {
  try {
    var tokenUrl =
        'http://jaki-taxi.com/admin/LatestApi/RestFul/${servicesType}/DriverLogout.php?' +
            'DeviceID=$devId&' +
            'DriverID=$driverId&' +
            'token=$token';
    var request = await get(tokenUrl);
    var data = jsonDecode(request.body);
    print(tokenUrl);
    print(data);
  } catch (e) {
    print(e.toString());
    tostDialog("خطأ في تسجيل الدخول");
  }
}


//login
Future<UserData> loginAPI(
    {userName, passWord,var devId, lang, devType, driType}) async {
  print("TYPE: " + driType);
  await PrefManager.setUserType(driType);
  var token = await PrefManager.getFireBaseToken();
  devId = token;
  var data;
  try {
    var driverUrl =
        '$baseUrl/driver_login?username=$userName&password=$passWord&devid=$devId&Lang=$lang&Devicetype=$devType';
    var waitatUrl =
        '$baseUrl/water_driver_login?username=$userName&password=$passWord&devid=$devId&Lang=$lang&Devicetype=$devType';
    var TransUrl =
        '$baseUrl/transportation_driver_login?username=$userName&password=$passWord&devid=$devId&Lang=$lang&Devicetype=$devType';
    var request = await get(driType == 'Trans'
        ? TransUrl
        : driType == 'Waitat'
            ? waitatUrl
            : driverUrl);

    data = jsonDecode(request.body);
    print(request.request.url);
    if (data["status"] == "success") {
      Map finalData = data["Driver_detail"][0];
      UserData userData = UserData(
        id: finalData["id"],
        type: finalData["type"],
        status: finalData["status"],
        email: finalData["email"],
        password: finalData["password"],
        gender: finalData["gender"],
        image: finalData["image"],
        name: finalData["name"],
        message: finalData["message"],
        phone: finalData["phone"],
        address: finalData["address"],
        bank_name: finalData["bank_name"],
        car_image: finalData["car_image"],
        Car_Make: finalData["Car_Make"],
        Car_Model: finalData["Car_Model"],
        car_no: finalData["car_no"],
        car_type: finalData["car_type"],
        City: finalData["City"],
        countCode: finalData["countCode"],
        device_token: finalData["device_token"],
        devid: finalData["devid"],
        Devicetype: finalData["Devicetype"],
        dob: finalData["dob"],
        driverrefr: finalData["driverrefr"],
        flag: finalData["flag"],
        hijri_date: finalData["hijri_date"],
        iban_account: finalData["iban_account"],
        Insurance: finalData["Insurance"],
        Lang: finalData["Lang"],
        Last_loc_update: finalData["Last_loc_update"],
        last_login: finalData["last_login"],
        license_no: finalData["license_no"],
        license_plate: finalData["license_plate"],
        Lieasence_Expiry_Date: finalData["Lieasence_Expiry_Date"],
        message2: finalData["message2"],
        national_id: finalData["national_id"],
        rating: finalData["rating"],
        regdate: finalData["regdate"],
        reserve: finalData["reserve"],
        Seating_Capacity: finalData["Seating_Capacity"],
        showCode: finalData["showCode"],
        socket_status: finalData["socket_status"],
        stc_pay: finalData["stc_pay"],
        surname: finalData["surname"],
        trip_options: finalData["trip_options"],
        trips_orders_all: finalData["trips_orders_all"],
        user_name: finalData["user_name"],
        user_status: finalData["user_status"],
        userCode: finalData["userCode"],
        vehicleSequenceNumber: finalData["vehicleSequenceNumber"],
        veichref: finalData["veichref"],
        wallet_amount: finalData["wallet_amount"],
        wasalCar: finalData["wasalCar"],
        wasalDriver: finalData["wasalDriver"],
      );

      String getServType() {
        if (driType == 'Trans') {
          return 'TransportationDrivers';
        } else if (driType == 'Waitat') {
          return 'TruckDrivers';
        } else {
          return 'Drivers';
        }
      }

      await onSaveToken(
        driverId: userData.id,
        servicesType: getServType(),
        token: userData.device_token,
        devId: token,
      );

      await PrefManager.setPrefData(userData);
      return userData;
    } else {
      tostDialog(data['message']);
    }
  } catch (e) {
    tostDialog(data['message']);
  }
  return null;
}

//Register
Future<UserData> registerDriverAPI(
    {File file,
    firstName,
    lastName,
    eMail,
    phoneNumber,
    passWord,
    drivType}) async {
  await PrefManager.setUserType(drivType);
  try {
    // String fileName = file.path.split('/').last;
    doz.FormData formData = doz.FormData.fromMap(
        //"file": await MultipartFile.fromFile(file.path, filename:fileName),
        {
          "username": "$firstName$lastName",
          "email": "$eMail",
          "phone": "$phoneNumber",
          "password": "$passWord",
          "surname": "$lastName",
          "name": "$firstName",
          "iban": "",
          "bank_name": "",
          "trip_options": "",
          "license_no": "",
          "Lieasence_Expiry_Date": "",
          "license_plate": "",
          "Insurance": "",
          "vehicleSequenceNumber": "",
          "Seating_Capacity": "",
          "Car_Model": "",
          "Car_Make": "",
          "car_no": "",
          "car_type": "",
          "address": "",
          "City": "",
          "gender": "Male",
          "dob": "",
          "national_id": "",
          "hijri_date": "",
          "stc_pay": "",
          "image": "",
          "car_image": "",
          "devid": "",
          "userCode": ""
        });
    var driverUrl = '$baseUrl/driver_sign_up';
    var waitatUrl = '$baseUrl/water_driver_sign_up';
    var transUrl = '$baseUrl/transportation_driver_sign_up';

    var response = await dio.post(
        drivType == 'Trans'
            ? transUrl
            : drivType == 'Waitat'
                ? waitatUrl
                : driverUrl,
        data: formData);
    print(response.request.uri);

    var body = json.decode(response.data);
    print(body);
    if (body["status"] == "success") {
      Map finalData = body["Driver_detail"][0];
      UserData userData = UserData(
        id: finalData["id"],
        type: finalData["type"],
        status: finalData["status"],
        email: finalData["email"],
        password: finalData["password"],
        gender: finalData["gender"],
        image: finalData["image"],
        name: finalData["name"],
        message: finalData["message"],
        phone: finalData["phone"],
        address: finalData["address"],
        bank_name: finalData["bank_name"],
        car_image: finalData["car_image"],
        Car_Make: finalData["Car_Make"],
        Car_Model: finalData["Car_Model"],
        car_no: finalData["car_no"],
        car_type: finalData["car_type"],
        City: finalData["City"],
        countCode: finalData["countCode"],
        device_token: finalData["device_token"],
        devid: finalData["devid"],
        Devicetype: finalData["Devicetype"],
        dob: finalData["dob"],
        driverrefr: finalData["driverrefr"],
        flag: finalData["flag"],
        hijri_date: finalData["hijri_date"],
        iban_account: finalData["iban_account"],
        Insurance: finalData["Insurance"],
        Lang: finalData["Lang"],
        Last_loc_update: finalData["Last_loc_update"],
        last_login: finalData["last_login"],
        license_no: finalData["license_no"],
        license_plate: finalData["license_plate"],
        Lieasence_Expiry_Date: finalData["Lieasence_Expiry_Date"],
        message2: finalData["message2"],
        national_id: finalData["national_id"],
        rating: finalData["rating"],
        regdate: finalData["regdate"],
        reserve: finalData["reserve"],
        Seating_Capacity: finalData["Seating_Capacity"],
        showCode: finalData["showCode"],
        socket_status: finalData["socket_status"],
        stc_pay: finalData["stc_pay"],
        surname: finalData["surname"],
        trip_options: finalData["trip_options"],
        trips_orders_all: finalData["trips_orders_all"],
        user_name: finalData["user_name"],
        user_status: finalData["user_status"],
        userCode: finalData["userCode"],
        vehicleSequenceNumber: finalData["vehicleSequenceNumber"],
        veichref: finalData["veichref"],
        wallet_amount: finalData["wallet_amount"],
        wasalCar: finalData["wasalCar"],
        wasalDriver: finalData["wasalDriver"],
      );


      await PrefManager.setPrefData(userData);
      return userData;
    } else {
      tostDialog("حدث خطأ اثناء التسجيل");
    }
  } catch (e) {
    tostDialog("حدث خطأ اثناء التسجيل!");
  }
  return null;
}

Future forgetPassAPI(mail, type) async {
  var data;
  try {
    var url =
        '${type == 'Trans' ? driTUrl : type == 'Waitat' ? driWUrl : driNUrl}/ForgetUserPassword.php?Email=$mail';
    var request = await get(url);
    data = jsonDecode(request.body);
    print(url);
    print(data);
    if (data["success"] == 1) {
      return data["message"];
    }
  } catch (e) {
    print(data);
    tostDialog("هذا البريد غير موجود");
  }
  return null;
}

//Update Profile
Future updateAPI({name, dId, passWord, phone, token, type}) async {
  var url =
      '${type == 'Trans' ? driTUrl : type == 'Waitat' ? driWUrl : driNUrl}/UpdateDriverData.php?token=$token&DriverID=$dId&Name=$name&Phone=$phone&Password=$passWord';
  try {
    var request = await get(url);
    var data = jsonDecode(request.body);
    print(data);
    if (data["success"] == 1) {
      tostDialog("تم تحديث البيانات");
      print(url);
      return true;
    }
  } catch (e) {
    print(url);
    tostDialog("خطأ في تحديث البيانات");
  }
}

//UserDATA
Future getData() async {
  try {
    var url =
        "${PrefManager.currentUser.type == 'Trans' ? driTUrl : PrefManager.currentUser.type == 'Waitat' ? driWUrl : driNUrl}/ViewDriversDetails.php?DriverID=${PrefManager.currentUser.id}&token=${PrefManager.currentUser.device_token}";
    print(url);
    var request = await get(url);
    Map data = jsonDecode(request.body);
    print(data);
    await PrefManager.setDriverId(data['AllDrivers'][0]['id']);
    List<String> userData = [];
    userData.add(data["AllDrivers"][0]["name"]);
    userData.add(data["AllDrivers"][0]["phone"]);
    userData.add(data["AllDrivers"][0]["name"]);
    userData.add(data["AllDrivers"][0]["surname"]);
    userData.add(data["AllDrivers"][0]["email"]);
    if (data["success"] == 1) {
      return userData;
    }
  } catch (e) {
    tostDialog("خطأ في الاتصال تآكد من اتصالك بالانترنت");
  }

}

//UserLocation
Future<List<TripModel>> getUserPendingTrips() async {
  List<TripModel> pendingTrips = List();
  try {
    var url =
        "${PrefManager.currentUser.type == 'Trans' ? driTUrl : PrefManager.currentUser.type == 'Waitat' ? driWUrl : driNUrl}/ViewPendingTrips.php?DriverID=${PrefManager.currentUser.id}&token=${PrefManager.currentUser.device_token}";
    var request = await get(url);
    var data = jsonDecode(request.body);
    if (data["success"] == 1) {
      List pendingTripsData = data["AllTrip"];
      //print("Pending Trips: " + data.toString());
      pendingTrips
          .addAll(pendingTripsData.map((trip) => TripModel.fromJSON(trip)));
      //print("NORMAL TRIP: " + pendingTrips[0].userName);
    }
  } catch (e) {
    print(e.toString());
    tostDialog("خطأ في الاتصال تآكد من اتصالك بالانترنت");
  }
  return pendingTrips;
}

//Users Completed Trips
Future getUserLocationsCompleted() async {
  try {
    var url =
        "${PrefManager.currentUser.type == 'Trans' ? driTUrl : PrefManager.currentUser.type == 'Waitat' ? driWUrl : driNUrl}/ViewCompletedTrips.php?DriverID=${PrefManager.currentUser.id}&token=${PrefManager.currentUser.device_token}";
    var request = await get(url);
    var data = jsonDecode(request.body);

    if (data["success"] == 1) {
      print(data);
      return data['AllTrip'];
    }
  } catch (e) {
    tostDialog("خطأ في الاتصال تآكد من اتصالك بالانترنت");
  }
}

//Push Driver Location
Future pushDriverLocation(lat, lng) async {

  try {
    var url =
        "${PrefManager.currentUser.type == 'Trans' ? driTUrl : PrefManager.currentUser.type == 'Waitat' ? driWUrl : driNUrl}/UpdateDriverLocations.php?DriverID=${PrefManager.currentUser.id}&DriverLat=$lat&DriverLon=$lng&token=${PrefManager.currentUser.device_token}";
    print(lat + lng);
    var request = await get(url);

    var data = jsonDecode(request.body);
    print("xzxzxzxzxz${url}");
    print("xzxzxzxzxz${request.body}");

    if (data["success"] == 1) {
      print("zxzxzxzxzxzx${data}");
      return data;
    }
  } catch (e) {
    tostDialog("خطأ في الاتصال تآكد من اتصالك بالانترنت");
  }
}

//Driver accept trip
Future<List> driverAcceptTrip(driverId, token, tripId) async {
  try {
    var url =
        "$driNUrl/DriverAcceptTrip.php?DriverID=$driverId&token=$token&TripID=$tripId";
    var request = await get(url);
    var data = jsonDecode(request.body);
    if (data["success"] == 1) {
      return [true, data["message"]];
    } else {
      return [false, data["message"]];
    }
  } catch (e) {
    return [false, "خطأ في الاتصال تآكد من اتصالك بالانترنت"];
  }
}

//non-Driver accept trip
Future nondriverAcceptTrip(driverId, token, tripId, price, type) async {
  try {
    var url =
        "${type == 'Trans' ? driTUrl : driWUrl}/NewOfferPrice.php?DriverID=$driverId&TripID=$tripId&Price=$price&token=$token";
    print(url);

    var request = await get(url);
    var data = jsonDecode(request.body);
    if (data["success"] == 1) {
      print(data);
      return data;
    }
  } catch (e) {
    tostDialog("خطأ في الاتصال تآكد من اتصالك بالانترنت");
  }
}

// details for trips
Future tripsDetails() async {
  try {
    var currentUrl ;
    print('55555${PrefManager.currentUser.type}');
    if(PrefManager.currentUser.type == 'Trans'){
      currentUrl = driTUrl;
      print('reach');
    }else if(PrefManager.currentUser.type == 'Waitat'){
      currentUrl = driWUrl;
      print('reach2');

    }else{
      currentUrl = driNUrl;
      print('reach3');

    }

    var url = "${currentUrl}/ViewCurrentTrips.php?DriverID=${PrefManager.currentUser.id}&token=${PrefManager.currentUser.device_token}";
    var request = await get(url);
    print("CURRENT: " + url);
    var data = jsonDecode(request.body);
    if (data["success"] == 1) {
      print(data['AllTrip']);
      return data['AllTrip'];
    }
  } catch (e) {
    tostDialog("خطأ في الاتصال تآكد من اتصالك بالانترنت");
  }
}

//Driver Start trip
Future driverStartTrip(driverId, token, tripId, type) async {
  try {
    var url =
        "${type == 'Trans' ? driTUrl : type == 'Waitat' ? driWUrl : driNUrl}/DriverStartTrip.php?DriverID=$driverId&token=$token&TripID=$tripId";
    var request = await get(url);
    var data = jsonDecode(request.body);
    if (data["success"] == 1) {
      print(data);
      return data;
    }
  } catch (e) {
    tostDialog("خطأ في الاتصال تآكد من اتصالك بالانترنت");
  }
}

//Driver Cancel trip
Future driverCancelTrip(driverId, token, tripId, type) async {
  try {
    var url =
        "${type == 'Trans' ? driTUrl : type == 'Waitat' ? driWUrl : driNUrl}/DriverCancelTrip.php?DriverID=$driverId&token=$token&TripID=$tripId";
    var request = await get(url);
    var data = jsonDecode(request.body);
    if (data["success"] == 1) {
      print(data);
      return data;
    }
  } catch (e) {
    tostDialog("خطأ في الاتصال تآكد من اتصالك بالانترنت");
  }
}

//Driver arrived
Future driverArrived(driverId, token, tripId, type) async {
  try {
    var url =
        "${type == 'Trans' ? driTUrl : type == 'Waitat' ? driWUrl : driNUrl}/DriverArrived.php?DriverID=$driverId&token=$token&TripID=$tripId";
    var request = await get(url);
    var data = jsonDecode(request.body);
    if (data["success"] == 1) {
      print(data);
      return data;
    }
  } catch (e) {
    tostDialog("خطأ في الاتصال تآكد من اتصالك بالانترنت");
  }
}

//Driver EndTrip
Future driverEndTrip(driverId, token, tripId, dropLat, lngLng, type) async {
  try {
    var url =
        "${type == 'Trans' ? driTUrl : type == 'Waitat' ? driWUrl : driNUrl}/DriverEndTrip.php?TripID=$tripId&drop_lat=$dropLat&drop_long=$lngLng&DriverID=$driverId&token=$token&";
    var request = await get(url);
    var data = jsonDecode(request.body);
    if (data["success"] == 1) {
      print(data);
      return data;
    }
  } catch (e) {
    tostDialog("خطأ في الاتصال تآكد من اتصالك بالانترنت");
  }
}

//UserStati
Future driverStatistics() async {
  try {
    var url =
        "$driNUrl/ViewDriverStatistics.php?DriverID=${PrefManager.currentUser.id}";
    print(url);
    var request = await get(url);
    var data = jsonDecode(request.body);
    print(data);
    List<num> userData = [];
    userData.add(data["rate"]);
    userData.add(data["TripsCount"]);
    userData.add(data["TripsCountOfToday"]);
    userData.add(data["TotalEarning"]);
    userData.add(data["TotalEarningOfToday"]);
    if (data["success"] == 1) {
      return userData;
    }
  } catch (e) {
    tostDialog("خطأ في الاتصال تآكد من اتصالك بالانترنت");
  }
}

//Terms
Future termsAPI() async {
  try {
    var regUrl = '$generalUrl/PrivacyPolicy.php';
    var request = await get(regUrl);
    var data = jsonDecode(request.body);
    print(data);
    if (data["success"] != 0) {
      return data;
    }
  } catch (e) {
    tostDialog("خطأ في احضار البيانات");
  }
}

//Banks
Future banksAPI() async {
  try {
    var regUrl = '$generalUrl/ViewBanks.php';
    var request = await get(regUrl);
    var data = jsonDecode(request.body);
    print(data);
    if (data["success"] != 0) {
      return data['AllBanks'];
    }
  } catch (e) {
    tostDialog("خطأ في احضار البيانات");
  }
}

//Places
Future placesAPI() async {
  try {
    var regUrl = '$generalUrl/ViewAllCity.php';
    var request = await get(regUrl);
    var data = jsonDecode(request.body);
    print(data);
    if (data["success"] != 0) {
      return data['AllCities'];
    }
  } catch (e) {
    tostDialog("خطأ في احضار البيانات");
  }
}

//Placescats
Future placescatsAPI() async {
  try {
    var regUrl = '$generalUrl/ViewPlacesCats.php';
    var request = await get(regUrl);
    var data = jsonDecode(request.body);
    print(data);
    if (data["success"] != 0) {
      return data['AllCats'];
    }
  } catch (e) {
    tostDialog("خطأ في احضار البيانات");
  }
}

//get Trip Price
Future getTripPrice(tripId, userId, token, type) async {
  try {
    var url =
        "${type == 'Trans' ? driTUrl : type == 'Waitat' ? driWUrl : driNUrl}GetTripCost.php?TripID=$tripId&DriverID=$userId&token=$token";
    var request = await get(url);
    var data = jsonDecode(request.body);
    if (data["success"] == 1) {
      print(data);
      return data;
    }
  } catch (e) {
    tostDialog("خطأ في الاتصال تآكد من اتصالك بالانترنت");
  }
}

//Report User
Future reportUser(driverId, userId, Text, token, type) async {
  try {
    var url =
        "${type == 'Trans' ? driTUrl : type == 'Waitat' ? driWUrl : driNUrl}/ReportDriver.php?DriverID=$driverId&UserID=$userId&Text=$Text&token=$token";
    var request = await get(url);
    var data = jsonDecode(request.body);
    return data;
  } catch (e) {
    tostDialog("خطأ في الاتصال تآكد من اتصالك بالانترنت");
  }
}
