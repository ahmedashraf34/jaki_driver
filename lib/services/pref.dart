import'package:jaki_kapten/services/exports.dart';

tostDialog(String message) {
  return Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
      timeInSecForIosWeb: 1,
      backgroundColor: Color(0xff222222),
      textColor: Colors.white,
      fontSize: 18.0);
}