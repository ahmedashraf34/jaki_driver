import 'package:location/location.dart';

class StaticFunctions{
  static double lat = 0.0, lng = 0.0;

  static Future<void> getLocation() async {
    print("location start");
    Location location = Location();
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    LocationData _locationData;

    try {
      _serviceEnabled = await location.serviceEnabled();
      if (!_serviceEnabled) {
        _serviceEnabled = await location.requestService();
        if (!_serviceEnabled) {}
      }
      _permissionGranted = await location.hasPermission();
      if (_permissionGranted == PermissionStatus.DENIED) {
        _permissionGranted = await location.requestPermission();
      }

      _locationData = await location.getLocation();

      print("location end");
      lat = _locationData.latitude;
      lng = _locationData.longitude;
    } catch (e) {
      print(e);
    }
    return null;
  }
}