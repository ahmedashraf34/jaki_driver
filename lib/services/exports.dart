//MAIN
export 'package:flutter/material.dart';
//Services
export 'dart:convert';
export 'dart:async';
export 'dart:io';
//Folders
export 'package:jaki_kapten/Component/shareWedget.dart';
export 'package:jaki_kapten/ToolsApp/app_Colors.dart';
export 'package:jaki_kapten/FrontEnd/sliders/intro_man.dart';
export 'package:jaki_kapten/services/Models/location.dart';
export 'package:jaki_kapten/FrontEnd/sliders/intro_four.dart';
export 'package:jaki_kapten/FrontEnd/sliders/intro_one.dart';
export 'package:jaki_kapten/FrontEnd/sliders/intro_three.dart';
export 'package:jaki_kapten/FrontEnd/sliders/intro_two.dart';
export 'package:jaki_kapten/FrontEnd/account/login.dart';
export 'package:jaki_kapten/FrontEnd/account/register.dart';
export 'package:jaki_kapten/FrontEnd/account/tabregister.dart';
export 'package:jaki_kapten/services/pref.dart';
export 'package:jaki_kapten/FrontEnd/home/home_one.dart';
export 'package:jaki_kapten/Component/Drawer_App.dart';
export 'package:jaki_kapten/FrontEnd/messages/message.dart';
export 'package:jaki_kapten/FrontEnd/messages/notifications.dart';
export 'package:jaki_kapten/services/Models/UserLocation.dart';
export 'package:jaki_kapten/FrontEnd/trip/current_trips.dart';
export 'package:jaki_kapten/FrontEnd/trip/trip_completed.dart';
export 'package:jaki_kapten/FrontEnd/trip/trip_penddeng.dart';
export 'package:jaki_kapten/splash.dart';

//Packages
export 'package:page_indicator/page_indicator.dart';
export 'package:fluttertoast/fluttertoast.dart';
export 'package:shared_preferences/shared_preferences.dart';
export 'package:http/http.dart';
export 'package:firebase_messaging/firebase_messaging.dart';
export 'package:google_maps_flutter/google_maps_flutter.dart';
export 'package:avatar_glow/avatar_glow.dart';
export 'package:jaki_kapten/services/config.dart';

export 'package:cloud_firestore/cloud_firestore.dart';
export 'package:jaki_kapten/prefs/pref_manager.dart';