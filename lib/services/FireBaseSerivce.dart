import 'dart:io';
import 'package:bot_toast/bot_toast.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:jaki_kapten/prefs/pref_manager.dart';

import 'Locator.dart';

bool isChat = false;

class FireBaseSerivce {

  final FirebaseMessaging _fcm = FirebaseMessaging();
  var rx = locator<PublishSubject<Map>>();
  var rxRefreshHome = locator<PublishSubject<bool>>();

  Future initialise(BuildContext context) async {
    if (Platform.isIOS) {
      await _fcm.requestNotificationPermissions(
        IosNotificationSettings(),
      );
    }

    await _fcm.getToken().then((mPushToken) async {
      print("Android Token : $mPushToken");
       PrefManager.setFireBaseToken(mPushToken);
      var toke = await PrefManager.getFireBaseToken();
      print("tokeeen${toke}");
    });


    _fcm.configure(
      onMessage: (Map<String, dynamic> message) async {
        var key = Platform.isIOS ? "notification" : "data";

        print("current=====key==== ${key}");

        if(Platform.isIOS){
          if(message["type"] == "chat"){
            if (isChat) {
              var text = message["message"];
              var time = message["created"];
              var newMap = {
                "text": text,
                "time": time,
              };
              rx.add(newMap);
            } else {
              onShowModelSheet("${message["message"]}", "${message["body"]}");
            }
          }else{
            onShowModelSheet("${message["title"]}", "${message["body"]}");
          }
        }else{
          if (message["data"]["type"] == "chat") {
            if (isChat) {
              var text = message["data"]["message"];
              var time = message["data"]["created"];
              var newMap = {
                "text": text,
                "time": time,
              };
              rx.add(newMap);
            } else {
              onShowModelSheet("${message["notification"]["message"]}", "${message["notification"]["body"]}");
            }
          } else {
            if(message['data']['body'].toString().contains('جديد')){
              rxRefreshHome.add(true);
            }
            onShowModelSheet("${message["notification"]["title"]}", "${message["notification"]["body"]}");
          }
        }
        print("onMessage: $message");
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
      },
    );
  }

  onShowModelSheet(String text, String title,
      {String productId, bool click = false}) {
    return BotToast.showNotification(
      enableSlideOff: true,
      onlyOne: true,
      subtitle: (_) => Text(
        text,
        style: TextStyle(
          fontFamily: "cairo",
        ),
      ),
//      onClose: (){
//        if(pop){
//          Navigator.pop(context);
//        }
//      },
      onTap: () {
        if (click) {}
      },
      crossPage: true,
      leading: (_) => Icon(
        Icons.notifications_active,
        color: Colors.blue,
        size: 30,
      ),
      animationDuration: Duration(milliseconds: 500),
      animationReverseDuration: Duration(milliseconds: 500),
      duration: Duration(seconds: 3),
      title: (_) => Text(
        title,
        style: TextStyle(
          fontFamily: "cairo",
        ),
      ),
    );
  }
}
